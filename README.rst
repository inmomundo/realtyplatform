Real Estate Web Platform
=======================

Resumen
-------

Plataforma de Clasificados con especial enfoque en Real Estate. Una plataforma
com multiples facetas para encontrar, comprar o vender propiedades. Una
plataforma web en la que propietarios, agentes o equipos de agentes y agencias
esten disponibles para servir y comunicarse con los clientes. Brindandoles a
una experiencia de usuario que no permita conecctar con los visitantes
brindadoles resultados de forma eficaz.

Una plataforma que integra Gestión de Contenidos (CMS) con Listas de Clasificados,
haciendo más competitivos a Agentes, Equipos o Agencias de Real Estate.
Un primer componente de una plataforma de Marketing Digital.
Posibilitandoles tener su propia presencia en internet, con diseño profesional
que les permita acercarse a los compradores. 

Una plataforma que no solo permita mostrar y recibir información, sino que se
integre como herramienta de trabajo, un compomente del negocio en Real Estate.
Una solución CRM (Customer Relationship Management) que sea más predictivo que
reactivo. Una herramienta que permita dar seguimiento a los prospectos desde su
captación hasta el cierre de las ventas y despues. Uncorporando al flujo del
proceso de negocios herramientas que faciliten el hacer perfiles claros de los
potenciales compradores, de los intereses del mercado y de oportunidades de 
negocio en segmentos desatendidos. Todo esto mediante herramientas gráficas de
fácil comprensión.

Etiquetas
---------

Real Estate, Software as a Service, Lead Generation, CRM, Leads Management, Inbound Marketing, Digital Advertising

Caracteristicas
---------------


General Classifieds (Basic Features)
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
- Gestión de Contactos
- Estadísticas
- Localizacion
    - Mapas
    - Ubicaciones
- Mapas
- Ubicaciones
- Categorias
    - Por sitio
    - Alias de Categorías
- Por sitio
- Alias de Categorías
- Notificaciones
    - Multiple Channels
- Multiple Channels
- Mensajería
- Public WebSite
    - Listas de Anuncios
        - mapas
        - Fichas
        - Galería
    - Herramientas
        - Conversor de medidas
        - Calculador de hipoteca
        - Comparación de Items (caracteristicas comunes)
    - Búsqueda
        - Textual
        - Filtros Avanzados
        - GeoEspacial
        - Entorno y Facilidades
            - Vecindario
            - Amenities
            - Accesibilidad
    - Multiples Vistas
        - Lista
        - Mapa
        - Fichas
    - Base de conocimiento
        - FAQ
        - Wiki
    - Recursos Relacionados
        - Diseño de Interiores
        - Diseño y Remodelación
- Multi-Lenguaje
    - UI Translations
    - Contenido Multilenguaje
- Search & Sort
    - Ranking
    - Algoritmo de Colonia de Hormigas
    - Algoritmo de Colmena
    - Algoritmo de Firefly
- Diseño Adaptativo
- Clasificado
    - Items Comparison
    - Comments
    - Syndication
        - RSS
        - ATOM
    - Meta Información
    - Social Networking
    - Campos Personalizados
    - Mutiples Categorías
    - Multiples Localidades


Realty Classifieds
~~~~~~~~~~~~~~~~~~
- Import and Export
    - RETS
    - XML
    - CSV
- Analitycs
    - Lead Activity
    - Property Property
    - Property Stats
        - Views
        - Visits
        - Comments
        - Mentions
- Leads
    - Lead Capture
    - Lead Profiling


Content Management
~~~~~~~~~~~~~~~~~~
- Temas
- Media Library
- Dominios del Agente
- Seo Friendly
- Artículos y Recursos
- Listas de Resultados
- Soluciones
    - WordPress
    - Django-CMS
    - Blog Zinnia


Customer Relationship Management
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
- Import and Export
    - CSV
    - RSS
    - XML
- Calendar
- E-Mail Client
- Chat
- Lead Tracking
    - Lead Capture
    - Lead Conversion
    - Lead Profiling
- Market or Project Analysis
    - Elasticidad Demanda
    - Distribución Geogŕafica
    - Precios Limite
    - Percepción de Valor Límite
        - Ciclo de Mercado
        - Ciclo de Vida
        - Factores Financieros
        - Demanda proyectada
        - Factores de Locación


User Management
~~~~~~~~~~~~~~~
- Importación/Exportación
    - VCard
    - CSV
    - XML
- Single SignOn
    - Social Login
    - Native Auth
    - OpenID
- Permission
    - Groups
    - Roles
    - Permissions


Publicidad
~~~~~~~~~~
- Portafolio de Productos
    - Impresiones
    - Mercado Segmentado
    - Tasas de Conversion
- Tercerizada
- Venta Directa
    - Native Ads
    - Top Ads
    - Banner
    - Formatos Creativos
        - Galerías
        - Slider
        - Multimedia


3rd Party Integration
~~~~~~~~~~~~~~~~~~~~~
    - Public API


Administration
~~~~~~~~~~~~~~
- Supervisión
    - Activity Tracking
        - Búsqueda Parametrizada
        - Búsqueda de MetaInformación
    - Textual Language Analyze
        - Statistical Text Analysis
            - Words and Phrases Frequency
            - Synonyms Exchange
    - Black List
        - Palabras y Frases
        - IP
        - Email Address
- Quality Assurance (QA)
    - Automatización de acciones
    - Calendarización
    - Desencadenadores
    - Procesamiento en Paralelo
- AI Application


E-Commerce
~~~~~~~~~~
- Affiliate accounts.
- Multiple payment gateways
    - Credit Card
    - PayPal
    - Manual payment
    - Credits system
- Segmentación y  Targeting
