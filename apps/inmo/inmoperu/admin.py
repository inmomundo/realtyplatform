# vim: set fileencoding=utf-8 :
import models
from django.contrib import admin


class IndexAdmin(admin.ModelAdmin):

    list_display = ('table', 'count', 'year')


class CategoryAdmin(admin.ModelAdmin):

    list_display = (
        'id',
        'parent',
        'name',
        'solicitud',
        'slug',
        'title',
        'sequence',
    )
    search_fields = ('name', 'slug')
    prepopulated_fields = {'slug': ['name']}


class LocationAdmin(admin.ModelAdmin):

    list_display = (
        'id',
        'parent',
        'name',
        'sequence',
        'label_sequence',
        'label_sequence_search',
    )
    raw_id_fields = ('parent',)
    search_fields = ('name',)


class CurrencyAdmin(admin.ModelAdmin):

    list_display = ('id', 'parent', 'name', 'symbol')
    list_filter = ('parent',)
    search_fields = ('name',)


class MeasureAdmin(admin.ModelAdmin):

    list_display = ('id', 'symbol')


class RealtorAdmin(admin.ModelAdmin):

    list_display = (
        'id',
        # 'facebook_id',
        # 'google_id',
        'name',
        # 'password',
        'country',
        'web',
        'agency',
        'phone',
        'mobile',
        'email',
        # 'photo',
        # 'level',
        # 'credits',
        # 'classification',
        # 'pathws',
        'status',
        # 'taccount',
        'active',
        'last_login',
        # 'source',
        'is_logged_in',
        # 'cs',
        'is_locked',
    )
    raw_id_fields = ('country',)
    search_fields = ('name',)


class RealtyAdmin(admin.ModelAdmin):

    list_display = (
        'id',
        'category',
        'realtor',
        # 'activity',
        # 'cstatus',
        # 'geo_sequence',
        # 'geo_name',
        # 'idgeo',
        'main_location',
        # 'main_location_name',
        # 'main_location_sequence',
        'sub_location',
        # 'sub_location_name',
        # 'sub_location_sequence',
        # 'slocation',
        # 'slocationii',
        # 'otherlocations',
        # 'address',
        # 'photos',
        # 'photos_order',
        # 'googlemap',
        'videos',
        'title',
        # 'description',
        # 'content',
        # 'specific_details',
        # 'details',
        # 'phone',
        # 'mobile',
        'email',
        # 'total_area',
        # 'contructed_area',
        # 'last_rental_price',
        'rental_price',
        # 'last_sale_price',
        'sale_price',
        'creation_date',
        # 'creation_time',
        'main_currency_price',
        'alternative_currency_price',
        # 'time_update',
        # 'last_price_update',
        # 'last_time_update_hide',
        # 'last_time_update',
        # 'time_ranking',
        # 'last_time_ranking',
        # 'penally',
        # 'date',
        # 'time',
        'is_active',
        'currency',
        # 'property_number',
        # 'random_sort_1',
        # 'random_sort_2',
        # 'is_relase',
        # 'youtube',
        # 'idyoutube',
        # 'imgyoutube',
        # 'urlyoutube',
        # 'obs',
        # 'realty_rankin',
        # 'realty_rental_rankin',
        # 'in_trash',
        # 'trash_date',
        # 'is_from_source',
        # 'ptrash',
        # 'dateptrash',
        # 'social_score',
        # 'nodelete',
        # 'is_sentry',
        # 'whowhydelete',
        # 'deactivation_date',
        # 'whowhyinactive',
        # 'messages',
        # 'update_status',
        # 'esmlri',
        # 'is_feed',
        'is_fraud',
    )
    raw_id_fields = (
        'category',
        'realtor',
        'main_location',
        'sub_location',
        'currency',
    )


class DetailAdmin(admin.ModelAdmin):

    list_display = (
        'id',
        'name',
        'label',
        'help_text',
        'options',
        'type',
        'search',
        'in_item',
        'optional',
        'sort',
        'parent',
        'group',
        'position',
        'code_editor',
        'code_search',
        'svn',
        'sequence',
        'child',
        'associated',
        'alternative_label',
    )
    search_fields = ('name',)


class RealtyDetailValueAdmin(admin.ModelAdmin):

    list_display = (
        'realty',
        'fld_1',
        'fld_2',
        'fld_3',
        'fld_4',
        'fld_5',
        'fld_6',
        'fld_7',
        'fld_8',
        'fld_9',
        'fld_10',
        'fld_11',
        'fld_12',
        'fld_13',
        'fld_14',
        'fld_15',
        'fld_16',
        'fld_17',
        'fld_18',
        'fld_19',
        'fld_20',
        'fld_21',
        'fld_22',
        'fld_23',
        'fld_24',
        'fld_25',
        'fld_26',
        'fld_27',
        'fld_28',
        'fld_29',
        'fld_30',
        'fld_31',
        'fld_32',
        'fld_33',
        'fld_34',
        'fld_35',
        'fld_36',
        'fld_37',
        'fld_38',
        'fld_39',
        'fld_40',
        'fld_41',
        'fld_42',
        'fld_43',
        'fld_44',
        'fld_45',
        'fld_46',
        'fld_47',
        'fld_48',
        'fld_49',
        'fld_50',
        'fld_51',
        'fld_52',
        'fld_53',
        'fld_54',
        'fld_55',
        'fld_56',
        'fld_57',
        'fld_58',
        'fld_60',
        'fld_61',
        'fld_62',
        'fld_64',
        'fld_65',
        'fld_66',
        'fld_67',
        'fld_68',
        'fld_69',
        'fld_70',
        'fld_71',
        'fld_73',
        'fld_74',
        'fld_75',
        'fld_76',
        'fld_77',
        'fld_78',
        'fld_79',
    )
    list_filter = ('realty',)


class CommentAdmin(admin.ModelAdmin):

    list_display = (
        'id',
        'realty',
        'content',
        'ilike',
        'sequence',
        'comment_type',
        'creation_date',
        'is_active',
        'in_trash',
        'checked',
    )
    list_filter = ('realty',)


class ResourcesAdmin(admin.ModelAdmin):

    list_display = (
        'id',
        'realty',
        'description',
        'main',
        'name',
        'mime',
        'size',
        'field_date',
        'field_hash',
        'base',
        'estado',
    )
    raw_id_fields = ('realty',)
    search_fields = ('name',)


class MessageAdmin(admin.ModelAdmin):

    list_display = (
        'id',
        'messageid',
        'realty',
        'realtor',
        'email',
        'name',
        'telephone',
        'country',
        'creation_date',
        'send_date',
        'expired',
        'expired_bv',
        'expiration_date',
        'archived',
        'answered',
        'answer_date',
        'serialize',
        'content',
        'delete_record',
        'answer_su',
    )
    list_filter = ('realty', 'realtor', 'country')
    search_fields = ('name',)


class Messages2Admin(admin.ModelAdmin):

    list_display = (
        'id',
        'realty',
        'email',
        'creation_date',
        'answer',
        'send',
        'answer_date',
        'code',
        'nsend',
        'send_date',
        'end_b',
    )
    list_filter = ('realty', 'creation_date', 'answer_date', 'send_date')


class NotificationAdmin(admin.ModelAdmin):

    list_display = (
        u'id',
        'notification_type',
        'content',
        'notification_time',
        'notification_status',
    )


class SuggestionAdmin(admin.ModelAdmin):

    list_display = (
        'id',
        'text',
        'creation_date',
        'score_0',
        'score_1',
        'score_2',
        'status',
        'admin',
        'grpid',
    )


class SuggestionsStatsAdmin(admin.ModelAdmin):

    list_display = (
        u'id',
        'sugestion',
        'text',
        'creation_date',
        'score_1',
        'score_2',
    )
    list_filter = ('sugestion',)


class ZoneAdmin(admin.ModelAdmin):

    list_display = (u'id', 'location', 'location_slug', 'location_name')
    list_filter = ('location',)


class BannerAdmin(admin.ModelAdmin):

    list_display = (
        'id',
        'realtor',
        'banner_type',
        'zone',
        'content',
        'is_active',
        'impressions',
    )
    list_filter = ('realtor', 'zone')


class BannerZoneAdmin(admin.ModelAdmin):

    list_display = ('id', 'location', 'name')
    list_filter = ('location',)
    search_fields = ('name',)


class SponsorshipAdmin(admin.ModelAdmin):

    list_display = (
        'realty',
        'geo',
        'location',
        'locname',
        'category',
        'content',
        'rental_price',
        'sale_price',
        'is_active',
        'send',
    )
    list_filter = ('realty', 'category')


class SponsorshipCountryAdmin(admin.ModelAdmin):

    list_display = ('id', 'name')
    search_fields = ('name',)


class SponsorshipOrderAdmin(admin.ModelAdmin):

    list_display = (
        'id',
        'realty',
        'currency',
        'price',
        'creation_date',
        'sponsored_date',
        'name',
        'email',
        'status',
        'transactionid',
        'is_sponsored',
        'end_sponsored',
        'payment_method',
        'reactivation_date',
    )
    list_filter = ('realty',)
    search_fields = ('name',)


class SponsorshipSortAdmin(admin.ModelAdmin):

    list_display = ('id', 'serialize')


class SponsorshipStatCountryAdmin(admin.ModelAdmin):

    list_display = (
        u'id',
        'realty',
        'yearmonth',
        'campocrv',
        'campocri',
        'campouknv',
        'campoukni',
        'campovev',
        'campovei',
        'campousv',
        'campousi',
        'campogbv',
        'campogbi',
        'campocav',
        'campocai',
        'campoesv',
        'campoesi',
        'campogtv',
        'campogti',
        'campopav',
        'campopai',
        'campochv',
        'campochi',
        'campofrv',
        'campofri',
        'campohnv',
        'campohni',
        'campopev',
        'campopei',
        'campomxv',
        'campomxi',
        'campocnv',
        'campocni',
        'campohkv',
        'campohki',
        'camponiv',
        'camponii',
        'camposvv',
        'camposvi',
        'camporuv',
        'camporui',
        'campocov',
        'campocoi',
        'campodev',
        'campodei',
        'campokwv',
        'campokwi',
        'campotwv',
        'campotwi',
        'campoecv',
        'campoeci',
        'camponlv',
        'camponli',
        'campodov',
        'campodoi',
        'camponov',
        'camponoi',
        'campouav',
        'campouai',
        'campoauv',
        'campoaui',
        'campoarv',
        'campoari',
        'campoitv',
        'campoiti',
        'campobev',
        'campobei',
        'campovnv',
        'campovni',
        'campodkv',
        'campodki',
        'camposev',
        'camposei',
        'camporov',
        'camporoi',
        'camposgv',
        'camposgi',
        'campohuv',
        'campohui',
        'campokrv',
        'campokri',
        'campoclv',
        'campocli',
        'campopyv',
        'campopyi',
        'campophv',
        'campophi',
        'campoptv',
        'campopti',
        'campoeuv',
        'campoeui',
        'campobrv',
        'campobri',
        'campogpv',
        'campogpi',
        'campoatv',
        'campoati',
        'camposnv',
        'camposni',
        'campoilv',
        'campoili',
        'campoplv',
        'campopli',
        'campoawv',
        'campoawi',
        'campoprv',
        'campopri',
        'campoczv',
        'campoczi',
        'camposav',
        'camposai',
        'campobov',
        'campoboi',
        'campojpv',
        'campojpi',
        'campobzv',
        'campobzi',
        'camponzv',
        'camponzi',
        'campomuv',
        'campomui',
        'campociv',
        'campocii',
        'campoa2v',
        'campoa2i',
        'campoqav',
        'campoqai',
        'camposkv',
        'camposki',
        'campongv',
        'campongi',
        'campottv',
        'campotti',
        'campoaev',
        'campoaei',
        'campouyv',
        'campouyi',
        'campobgv',
        'campobgi',
        'campothv',
        'campothi',
        'campobwv',
        'campobwi',
        'campolvv',
        'campolvi',
        'campogrv',
        'campogri',
        'campotrv',
        'campotri',
        'campobav',
        'campobai',
        'campomav',
        'campomai',
        'campoghv',
        'campoghi',
        'campoafv',
        'campoafi',
        'campocuv',
        'campocui',
        'campoinv',
        'campoini',
        'campopfv',
        'campopfi',
        'campoaiv',
        'campoaii',
        'campobmv',
        'campobmi',
        'campomyv',
        'campomyi',
        'campoidv',
        'campoidi',
        'campotnv',
        'campotni',
        'campocmv',
        'campocmi',
        'camporsv',
        'camporsi',
        'campoaov',
        'campoaoi',
        'campofiv',
        'campofii',
        'campoadv',
        'campoadi',
        'campoluv',
        'campolui',
        'campokzv',
        'campokzi',
        'campodzv',
        'campodzi',
        'campoiev',
        'campoiei',
        'camposrv',
        'camposri',
        'campogev',
        'campogei',
        'camposiv',
        'camposii',
        'campoanv',
        'campoani',
        'campomzv',
        'campomzi',
        'campokhv',
        'campokhi',
        'campobdv',
        'campobdi',
        'campohtv',
        'campohti',
        'campoirv',
        'campoiri',
        'campokyv',
        'campokyi',
        'campobyv',
        'campobyi',
        'campoeev',
        'campoeei',
        'campozav',
        'campozai',
        'campohrv',
        'campohri',
        'campoyev',
        'campoyei',
        'campoltv',
        'campolti',
        'campojmv',
        'campojmi',
        'campomgv',
        'campomgi',
        'campoa1v',
        'campoa1i',
        'campomqv',
        'campomqi',
        'campogqv',
        'campogqi',
        'campomkv',
        'campomki',
        'campocyv',
        'campocyi',
        'campomcv',
        'campomci',
        'campotzv',
        'campotzi',
        'camporev',
        'camporei',
        'campobfv',
        'campobfi',
        'campoapv',
        'campoapi',
        'campoegv',
        'campoegi',
        'campomdv',
        'campomdi',
        'campoisv',
        'campoisi',
        'campobhv',
        'campobhi',
        'campoazv',
        'campoazi',
        'camponpv',
        'camponpi',
        'campokgv',
        'campokgi',
        'campodjv',
        'campodji',
        'campoamv',
        'campoami',
        'camponcv',
        'camponci',
        'campoiqv',
        'campoiqi',
        'campokev',
        'campokei',
        'campobbv',
        'campobbi',
        'campotcv',
        'campotci',
        'campouzv',
        'campouzi',
        'campobsv',
        'campobsi',
        'campoomv',
        'campoomi',
        'campopkv',
        'campopki',
        'campojov',
        'campojoi',
        'campolbv',
        'campolbi',
        'campoagv',
        'campoagi',
        'campogyv',
        'campogyi',
        'campovcv',
        'campovci',
        'camposyv',
        'camposyi',
        'campobnv',
        'campobni',
        'campopsv',
        'campopsi',
        'campomvv',
        'campomvi',
        'campomov',
        'campomoi',
        'campoalv',
        'campoali',
        'campolyv',
        'campolyi',
        'campoytv',
        'campoyti',
        'campomtv',
        'campomti',
        'campogfv',
        'campogfi',
        'campoviv',
        'campovii',
        'campolav',
        'campolai',
        'camposcv',
        'camposci',
        'campozmv',
        'campozmi',
        'campoggv',
        'campoggi',
        'campobjv',
        'campobji',
        'campogiv',
        'campogii',
        'campoetv',
        'campoeti',
        'campognv',
        'campogni',
        'campommv',
        'campommi',
        'campocvv',
        'campocvi',
        'campoliv',
        'campolii',
        'campomfv',
        'campomfi',
        'campogav',
        'campogai',
        'campostv',
        'camposti',
        'campolrv',
        'campolri',
        'campojev',
        'campojei',
        'campoknv',
        'campokni',
        'campogmv',
        'campogmi',
        'campogdv',
        'campogdi',
        'campolkv',
        'campolki',
        'campoguv',
        'campogui',
        'campozwv',
        'campozwi',
        'campotlv',
        'campotli',
        'campocgv',
        'campocgi',
        'campovav',
        'campovai',
        'campomrv',
        'campomri',
        'campotgv',
        'campotgi',
        'campolcv',
        'campolci',
    )


class SponsorshipStatDayAdmin(admin.ModelAdmin):

    list_display = (
        u'id',
        'realty',
        'yearmonth',
        'dayv01',
        'dayi01',
        'dayv02',
        'dayi02',
        'dayv03',
        'dayi03',
        'dayv04',
        'dayi04',
        'dayv05',
        'dayi05',
        'dayv06',
        'dayi06',
        'dayv07',
        'dayi07',
        'dayv08',
        'dayi08',
        'dayv09',
        'dayi09',
        'dayv10',
        'dayi10',
        'dayv11',
        'dayi11',
        'dayv12',
        'dayi12',
        'dayv13',
        'dayi13',
        'dayv14',
        'dayi14',
        'dayv15',
        'dayi15',
        'dayv16',
        'dayi16',
        'dayv17',
        'dayi17',
        'dayv18',
        'dayi18',
        'dayv19',
        'dayi19',
        'dayv20',
        'dayi20',
        'dayv21',
        'dayi21',
        'dayv22',
        'dayi22',
        'dayv23',
        'dayi23',
        'dayv24',
        'dayi24',
        'dayv25',
        'dayi25',
        'dayv26',
        'dayi26',
        'dayv27',
        'dayi27',
        'dayv28',
        'dayi28',
        'dayv29',
        'dayi29',
        'dayv30',
        'dayi30',
        'dayv31',
        'dayi31',
    )


class SponsorshipStatHourAdmin(admin.ModelAdmin):

    list_display = (
        u'id',
        'realty',
        'yearmonth',
        'hourv00',
        'houri00',
        'hourv01',
        'houri01',
        'hourv02',
        'houri02',
        'hourv03',
        'houri03',
        'hourv04',
        'houri04',
        'hourv05',
        'houri05',
        'hourv06',
        'houri06',
        'hourv07',
        'houri07',
        'hourv08',
        'houri08',
        'hourv09',
        'houri09',
        'hourv10',
        'houri10',
        'hourv11',
        'houri11',
        'hourv12',
        'houri12',
        'hourv13',
        'houri13',
        'hourv14',
        'houri14',
        'hourv15',
        'houri15',
        'hourv16',
        'houri16',
        'hourv17',
        'houri17',
        'hourv18',
        'houri18',
        'hourv19',
        'houri19',
        'hourv20',
        'houri20',
        'hourv21',
        'houri21',
        'hourv22',
        'houri22',
        'hourv23',
        'houri23',
    )


class SponsorshipDemoAdmin(admin.ModelAdmin):

    list_display = (
        'realty',
        'geo',
        'location',
        'locname',
        'category',
        'content',
        'rental_price',
        'sale_price',
        'is_active',
        'send',
    )
    list_filter = ('realty', 'category')


class UserAccessAdmin(admin.ModelAdmin):

    list_display = ('realtor', 'last_login', 'ip', 'ips')
    list_filter = ('realtor',)


class StatisticsAdmin(admin.ModelAdmin):

    list_display = (
        u'id',
        'ads_code',
        'country',
        'prints',
        'views',
        'day',
        'month',
        'year',
        'hours',
    )


class NavigationAdmin(admin.ModelAdmin):

    list_display = ('id', 'items')


class TableSettingAdmin(admin.ModelAdmin):

    list_display = (u'id', 'vary')


class EmailAdmin(admin.ModelAdmin):

    list_display = (
        'id',
        'email',
        'is_banned',
        'last_update',
        'sends',
        'is_pending',
    )


class DictionaryAdmin(admin.ModelAdmin):

    list_display = (
        'id',
        'object_id',
        'content_type',
        'language',
        'translation',
        'translation_alternative',
        'path',
    )


class IpCountryAdmin(admin.ModelAdmin):

    list_display = (
        u'id',
        'ip_from',
        'ip_to',
        'country_code2',
        'country_code3',
        'country_name',
    )


class PluginAdmin(admin.ModelAdmin):

    list_display = (
        'id',
        'name',
        'description',
        'events',
        'order',
        'plugin_type',
    )
    search_fields = ('name',)


class RegisterAdmin(admin.ModelAdmin):

    list_display = ('id', 'email', 'creation_date', 'status')


class ConverterAdmin(admin.ModelAdmin):

    list_display = ('id', 'exchange_rate', 'currency', 'last_update')


def _register(model, admin_class):
    admin.site.register(model, admin_class)


_register(models.Index, IndexAdmin)
_register(models.Category, CategoryAdmin)
_register(models.Location, LocationAdmin)
_register(models.Currency, CurrencyAdmin)
_register(models.Measure, MeasureAdmin)
_register(models.Realtor, RealtorAdmin)
_register(models.Realty, RealtyAdmin)
_register(models.Detail, DetailAdmin)
_register(models.RealtyDetailValue, RealtyDetailValueAdmin)
_register(models.Comment, CommentAdmin)
_register(models.Resources, ResourcesAdmin)
_register(models.Message, MessageAdmin)
_register(models.Messages2, Messages2Admin)
_register(models.Notification, NotificationAdmin)
_register(models.Suggestion, SuggestionAdmin)
_register(models.SuggestionsStats, SuggestionsStatsAdmin)
_register(models.Zone, ZoneAdmin)
_register(models.Banner, BannerAdmin)
_register(models.BannerZone, BannerZoneAdmin)
_register(models.Sponsorship, SponsorshipAdmin)
_register(models.SponsorshipCountry, SponsorshipCountryAdmin)
_register(models.SponsorshipOrder, SponsorshipOrderAdmin)
_register(models.SponsorshipSort, SponsorshipSortAdmin)
_register(models.SponsorshipStatCountry, SponsorshipStatCountryAdmin)
_register(models.SponsorshipStatDay, SponsorshipStatDayAdmin)
_register(models.SponsorshipStatHour, SponsorshipStatHourAdmin)
_register(models.SponsorshipDemo, SponsorshipDemoAdmin)
_register(models.UserAccess, UserAccessAdmin)
_register(models.Statistics, StatisticsAdmin)
_register(models.Navigation, NavigationAdmin)
_register(models.TableSetting, TableSettingAdmin)
_register(models.Email, EmailAdmin)
_register(models.Dictionary, DictionaryAdmin)
# _register(models.IpCountry, IpCountryAdmin)
_register(models.Plugin, PluginAdmin)
_register(models.Register, RegisterAdmin)
_register(models.Converter, ConverterAdmin)
