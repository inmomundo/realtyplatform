# -*- coding: utf-8 -*-
from __future__ import unicode_literals
import time

from django.db import models
# from compositekey import __future__, db


# =============================================================================
# General Classifieds
# =============================================================================
class Index(models.Model):
    table = models.CharField(db_column='Tabla', primary_key=True, max_length=20,
                             verbose_name='Tabla')
    count = models.BigIntegerField(db_column='Cont', verbose_name='Conteo')
    year = models.TextField(db_column='Year', verbose_name='Año')

    class Meta:
        managed = False
        db_table = "indices"
        verbose_name = "Indice"
        verbose_name_plural = "Indices"

    def __unicode__(self):
        return u'%s' % self.table


class Category(models.Model):
    id = models.BigIntegerField(db_column='Idcategoria', primary_key=True,
                                verbose_name='ID Categoria')
    parent = models.ForeignKey('self', db_column='Padre', blank=True,
                               null=True, verbose_name='Categoria Padre')
    name = models.CharField(db_column='Nombre', max_length=255,
                            verbose_name='Categoria')
    solicitud = models.CharField(db_column='Solicitud', max_length=255)
    slug = models.CharField(db_column='Path', max_length=255, blank=True,
                            null=True, verbose_name='Semantic URL')
    title = models.CharField(db_column='Titulo', max_length=255, blank=True,
                             null=True, verbose_name='Título')
    sequence = models.CharField(db_column='Secuencia', max_length=255,
                                blank=True, null=True, verbose_name='Sequencia')
    # details = models.ManyToManyField('Detail', through='CategoryDetail',
    #                                  through_fields=('category', 'detail'))

    class Meta:
        managed = False
        db_table = 'categorias'
        verbose_name = "Categoria"
        verbose_name_plural = "Categorias"

    def __unicode__(self):
        return u'%s' % self.name


class Location(models.Model):
    id = models.BigIntegerField(db_column='Id', primary_key=True)
    parent = models.ForeignKey('self', db_column='Padre', blank=True, null=True)
    name = models.CharField(db_column='Nombre', max_length=765,
                            verbose_name='Nombre')
    sequence = models.CharField(db_column='Secuencia', max_length=765,
                                blank=True, null=True, verbose_name='Secuencia')
    label_sequence = models.TextField(db_column='Secuencialabel',
                                      verbose_name='Etiqueta')
    label_sequence_search = models.TextField(db_column='Secuencialabelsearch',
                                             verbose_name='Etiqueta Busqueda')

    class Meta:
        managed = False
        db_table = 'locations'
        verbose_name = "Localidad"
        verbose_name_plural = "Localidades"

    def __unicode__(self):
        return u'%s' % self.name


class Currency(models.Model):
    id = models.BigIntegerField(db_column='Idmoneda', primary_key=True)
    parent = models.ForeignKey('self', db_column='Padre', null=True, blank=True)
    name = models.CharField(db_column='Nombre', max_length=20, blank=True,
                            null=True, verbose_name='Moneda')
    symbol = models.CharField(db_column='Simbolo', max_length=4, blank=True,
                              null=True, verbose_name='Símbolo Monetario')

    class Meta:
        managed = False
        db_table = 'monedas'
        verbose_name = "Moneda"
        verbose_name_plural = "Monedas"

    def __unicode__(self):
        return u'%s' % self.name


class Measure(models.Model):
    id = models.AutoField(db_column='Idmedida', primary_key=True)
    symbol = models.CharField(db_column='Simbolo', max_length=10, blank=True,
                              null=True, verbose_name='Simbolo')

    class Meta:
        managed = False
        db_table = 'medidas'
        verbose_name = "Unidad de Medida"
        verbose_name_plural = "Unidades de Medida"

    def __unicode__(self):
        return u'%s' % self.symbol


class Realtor(models.Model):
    REALTOR_STATUS = (
        ('N', 'Deshabilitado'),
        ('SB', 'Habilitado'),
        ('BL', 'Bloqueado')
    )
    REALTOR_ACTIVATION_ESTATUS = ((1, 'Active'), (0, 'DeActive'))
    REALTOR_LOGIN_ESTATUS = ((1, 'Logged In'), (0, 'Logged Out'))

    id = models.CharField(db_column='Idcorredor', primary_key=True, max_length=20)
    facebook_id = models.CharField(db_column='facebook_Id', max_length=250, blank=True, null=True)
    google_id = models.CharField(db_column='google_Id', max_length=250, blank=True, null=True)
    name = models.CharField(db_column='Nombre', max_length=100, verbose_name='Nombre')
    password = models.CharField(db_column='Password', max_length=75, blank=True, null=True)
    country = models.ForeignKey('Location', db_column='Idpais', verbose_name='País')
    web = models.URLField(db_column='Web', max_length=255, blank=True, null=True)
    agency = models.CharField(db_column='Empresa', max_length=255, blank=True, null=True,
                              verbose_name='Empresa')
    phone = models.CharField(db_column='Telefono', max_length=255, blank=True, null=True,
                             verbose_name='Teléfono')
    mobile = models.CharField(db_column='Celular', max_length=255, blank=True, null=True,
                              verbose_name='Celular')
    email = models.EmailField(db_column='Email', max_length=255)
    photo = models.TextField(db_column='Foto', blank=True, null=True, verbose_name='Foto')
    level = models.CharField(db_column='Nivel', max_length=1, blank=True, null=True,
                             verbose_name='Nivel')
    credits = models.FloatField(db_column='Credits', blank=True, null=True, verbose_name='Creditos')
    classification = models.IntegerField(db_column='Tipo', verbose_name='Tipo')
    pathws = models.CharField(max_length=10)
    status = models.CharField(max_length=2, blank=True, null=True, choices=REALTOR_STATUS,
                              default='N', verbose_name='Estado')
    taccount = models.IntegerField(db_column='Taccount', blank=True, null=True)
    active = models.IntegerField(choices=REALTOR_ACTIVATION_ESTATUS, default=1)
    last_login = models.BigIntegerField(db_column='fecha', default=int(time.time()))
    source = models.IntegerField(blank=True, null=True)
    is_logged_in = models.IntegerField(db_column='login', blank=True, null=True,
                                       choices=REALTOR_LOGIN_ESTATUS, default=0)
    cs = models.CharField(max_length=10, blank=True, null=True)
    is_locked = models.IntegerField(db_column='_lock', blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'corredores'
        verbose_name = "Corredor"
        verbose_name_plural = "Corredores"

    def __unicode__(self):
        return u'%s' % self.name


# FIXME: Este modelo puede ser divido en varios modelos como stats o meta-data
class Realty(models.Model):
    id = models.CharField(db_column='Idpropiedad', primary_key=True, max_length=30)
    category = models.ForeignKey('Category', db_column='Idcategoria')
    realtor = models.ForeignKey('Realtor', db_column='Idcorredor', max_length=20)
    activity = models.IntegerField(db_column='Actividad', blank=True, null=True)
    cstatus = models.CharField(max_length=2, blank=True, null=True)
    geo_sequence = models.CharField(db_column='Sequenciageo', max_length=255, blank=True, null=True)
    geo_name = models.CharField(db_column='Geoname', max_length=255, blank=True, null=True)
    idgeo = models.BigIntegerField(db_column='Idgeo', blank=True, null=True)  # old geo model
    main_location = models.ForeignKey('Location', db_column='Idlocation', verbose_name='Localidad')
    main_location_name = models.CharField(db_column='location', max_length=255, blank=True,
                                          null=True, verbose_name='Nombre de Localidad')
    main_location_sequence = models.CharField(db_column='Seqlocation', max_length=255)
    # old locationsii
    sub_location = models.ForeignKey('Location', db_column='Idlocationii', blank=True, null=True,
                                     verbose_name='Sub Localidad', related_name='+')
    sub_location_name = models.CharField(db_column='locationii', max_length=255, blank=True,
                                         null=True)
    sub_location_sequence = models.CharField(db_column='Seqlocationii', max_length=255, blank=True,
                                             null=True)
    slocation = models.CharField(max_length=255, blank=True, null=True)
    slocationii = models.CharField(max_length=255, blank=True, null=True)
    # use a CommaSeparatedIntegerField
    otherlocations = models.CharField(max_length=255, blank=True, null=True)
    address = models.CharField(max_length=255, blank=True, null=True, verbose_name='Dirección')
    # use a CommaSeparatedIntegerField
    photos = models.TextField(db_column='Fotos', blank=True, null=True, verbose_name='Fotos')
    photos_order = models.BigIntegerField(db_column='Orderfotos', blank=True, null=True,
                                          verbose_name='Orden')
    # use a real GoogleMarkerField
    googlemap = models.CharField(db_column='Googlemap', max_length=255, blank=True, null=True,
                                 verbose_name='Map Marker')
    videos = models.TextField(db_column='Videos', blank=True, null=True)
    title = models.CharField(db_column='Titulo', max_length=70, blank=True, null=True)
    description = models.TextField(db_column='Descripcion', blank=True, null=True)
    content = models.TextField(db_column='Content', blank=True, null=True)
    specific_details = models.TextField(db_column='Detalleespecifico', blank=True, null=True)
    details = models.TextField(db_column='Detail', blank=True, null=True)
    phone = models.CharField(db_column='Telefono', max_length=100, blank=True, null=True)
    mobile = models.CharField(db_column='Celular', max_length=100, blank=True, null=True)
    email = models.CharField(db_column='Email', max_length=255, blank=True, null=True)
    total_area = models.CharField(db_column='AT', max_length=100, blank=True, null=True,
                                  verbose_name='Área Total')
    contructed_area = models.CharField(db_column='AC', max_length=100, blank=True, null=True,
                                       verbose_name='Área Construida')
    last_rental_price = models.FloatField(db_column='_Lastpricea', blank=True, null=True)
    rental_price = models.FloatField(db_column='Palquiler', blank=True, null=True)
    last_sale_price = models.FloatField(db_column='_Lastpricev', blank=True, null=True)
    sale_price = models.FloatField(db_column='Pventa', blank=True, null=True)
    creation_date = models.CharField(db_column='Fechaing', max_length=30, blank=True, null=True)
    creation_time = models.BigIntegerField(db_column='Timeing')
    main_currency_price = models.FloatField(db_column='Pcurrency1', blank=True, null=True)
    alternative_currency_price = models.FloatField(db_column='Pcurrency2', blank=True, null=True)
    time_update = models.BigIntegerField(db_column='Timeupdate')
    last_price_update = models.BigIntegerField(db_column='_Lastupdatep', blank=True, null=True)
    last_time_update_hide = models.BigIntegerField(db_column='_Lasttimeupdate', blank=True,
                                                   null=True)
    last_time_update = models.BigIntegerField(db_column='Lasttimeupdate', blank=True, null=True)
    time_ranking = models.BigIntegerField(db_column='Timeranking', blank=True, null=True)
    last_time_ranking = models.BigIntegerField(db_column='Lasttimeranking', blank=True, null=True)
    penally = models.BigIntegerField(db_column='Penales', blank=True, null=True)
    date = models.CharField(db_column='Fecha', max_length=30)
    time = models.BigIntegerField(db_column='Tiempo', blank=True, null=True)
    is_active = models.CharField(db_column='Activado', max_length=1)
    currency = models.ForeignKey('Currency', db_column='Idmoneda')
    property_number = models.BigIntegerField(db_column='Nopropiedad')
    random_sort_1 = models.FloatField(db_column='OAleatorio', blank=True, null=True)
    random_sort_2 = models.FloatField(db_column='OAleatorioIII', blank=True, null=True)
    is_relase = models.IntegerField(db_column='liberado', blank=True, null=True)
    youtube = models.CharField(max_length=255, blank=True, null=True, verbose_name='YouTube')
    idyoutube = models.CharField(max_length=100, blank=True, null=True, verbose_name='YouTube Id')
    imgyoutube = models.CharField(max_length=255, blank=True, null=True,
                                  verbose_name='YouTube Image')
    urlyoutube = models.CharField(max_length=255, blank=True, null=True, verbose_name='YouTube URL')
    obs = models.TextField(blank=True, null=True)
    realty_rankin = models.BigIntegerField(db_column='Inmorankin', blank=True, null=True,
                                           verbose_name='Rental Ranking')
    realty_rental_rankin = models.BigIntegerField(db_column='Inmorankinalq', blank=True, null=True,
                                                  verbose_name='Rental Ranking')
    in_trash = models.IntegerField(db_column='trash', blank=True, null=True,
                                   verbose_name='Papelera')
    trash_date = models.BigIntegerField(db_column='datetrash', blank=True, null=True,
                                        verbose_name='Trash Date')
    is_from_source = models.IntegerField(db_column='source', blank=True, null=True,
                                         verbose_name='Tiene Fuente')
    ptrash = models.IntegerField(blank=True, null=True)
    dateptrash = models.IntegerField(blank=True, null=True)
    social_score = models.BigIntegerField(db_column='socialscore', blank=True, null=True)
    nodelete = models.IntegerField(blank=True, null=True)
    is_sentry = models.IntegerField(blank=True, null=True)
    whowhydelete = models.IntegerField(blank=True, null=True)
    deactivation_date = models.BigIntegerField(db_column='dateinactive', blank=True, null=True)
    whowhyinactive = models.IntegerField(blank=True, null=True)
    messages = models.BigIntegerField(blank=True, null=True)
    update_status = models.IntegerField(db_column='actualizado', blank=True, null=True)
    esmlri = models.IntegerField(blank=True, null=True)
    is_feed = models.SmallIntegerField(blank=True, null=True)
    is_fraud = models.SmallIntegerField(blank=True, null=True)
    # details = models.ManyToManyField('Detail', through='RealtyDetail',
    #                                  through_fields=('realty', 'detail'))

    class Meta:
        managed = False
        db_table = 'propiedades'
        verbose_name = "Propiedad"
        verbose_name_plural = "Propiedades"

    def __unicode__(self):
        return u'%s - %s' % (self.id, self.title)


class Detail(models.Model):
    """
    XXX: asociaciones desconocidas
    """
    id = models.BigIntegerField(primary_key=True, db_column='pkid')
    name = models.CharField(max_length=255, db_column='sname', blank=True, null=True,
                            verbose_name='Detalle')
    label = models.CharField(max_length=255, db_column='slabel', blank=True, null=True,
                             verbose_name='Etiqueta')
    help_text = models.CharField(max_length=255, db_column='shelp', blank=True, null=True,
                                 verbose_name='Help Text')
    options = models.TextField(db_column='soptions', blank=True, null=True, verbose_name='Opciones')
    type = models.IntegerField(db_column='itype', blank=True, null=True)
    search = models.IntegerField(db_column='binsearch', blank=True, null=True)
    in_item = models.IntegerField(db_column='binitem', blank=True, null=True)
    optional = models.IntegerField(db_column='boptional', blank=True, null=True)
    sort = models.BigIntegerField(db_column='isort', blank=True, null=True)
    parent = models.BigIntegerField(db_column='iparent', blank=True, null=True)
    group = models.BigIntegerField(db_column='igroup', blank=True, null=True)
    position = models.CharField(db_column='spos', max_length=255, blank=True, null=True)
    code_editor = models.TextField(db_column='sjsaddedit', blank=True, null=True)
    code_search = models.TextField(db_column='sjssearch', blank=True, null=True)
    svn = models.TextField(blank=True, null=True)
    sequence = models.CharField(db_column='seq', max_length=255, blank=True, null=True)
    child = models.IntegerField(db_column='bchildpre', blank=True, null=True)
    associated = models.BigIntegerField(db_column='iassociate', blank=True, null=True)
    alternative_label = models.CharField(db_column='slabelii', max_length=255, blank=True,
                                         null=True)

    class Meta:
        managed = False
        db_table = 'details'
        verbose_name = 'Detalle'
        verbose_name_plural = "Detalles"

    def __unicode__(self):
        return u'%s' % self.name


# FIXME: create a primary key
"""
class CategoryDetail(models.Model):
    # id = models.CompositeField("detail", "category", primary_key=True)
    detail = models.ForeignKey('Detail', db_column='fpkid')
    category = models.ForeignKey('Category', db_column='fkcategory')
    operations = models.CharField(db_column='soperations', max_length=255, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'detailscats'
        unique_together = (('detail', 'category'),)
"""


# FIXME: create a primary key 
"""
class RealtyDetails(models.Model):
    detail = models.ForeignKey('Detail', db_column='fkid')
    realty = models.ForeignKey('Realty', db_column='sidpropiedad', max_length=30)

    class Meta:
        managed = False
        db_table = 'detailsproperties'
        unique_together = (('detail', 'realty'),)
"""


class RealtyDetailValue(models.Model):
    realty = models.OneToOneField('Realty', db_column='sidpropiedad', primary_key=True, max_length=30)
    fld_1 = models.BigIntegerField(blank=True, null=True)
    fld_2 = models.BigIntegerField(blank=True, null=True)
    fld_3 = models.BigIntegerField(blank=True, null=True)
    fld_4 = models.BigIntegerField(blank=True, null=True)
    fld_5 = models.CharField(max_length=255, blank=True, null=True)
    fld_6 = models.IntegerField(blank=True, null=True)
    fld_7 = models.IntegerField(blank=True, null=True)
    fld_8 = models.IntegerField(blank=True, null=True)
    fld_9 = models.IntegerField(blank=True, null=True)
    fld_10 = models.IntegerField(blank=True, null=True)
    fld_11 = models.IntegerField(blank=True, null=True)
    fld_12 = models.IntegerField(blank=True, null=True)
    fld_13 = models.IntegerField(blank=True, null=True)
    fld_14 = models.IntegerField(blank=True, null=True)
    fld_15 = models.IntegerField(blank=True, null=True)
    fld_16 = models.IntegerField(blank=True, null=True)
    fld_17 = models.IntegerField(blank=True, null=True)
    fld_18 = models.IntegerField(blank=True, null=True)
    fld_19 = models.IntegerField(blank=True, null=True)
    fld_20 = models.IntegerField(blank=True, null=True)
    fld_21 = models.IntegerField(blank=True, null=True)
    fld_22 = models.IntegerField(blank=True, null=True)
    fld_23 = models.IntegerField(blank=True, null=True)
    fld_24 = models.IntegerField(blank=True, null=True)
    fld_25 = models.IntegerField(blank=True, null=True)
    fld_26 = models.IntegerField(blank=True, null=True)
    fld_27 = models.IntegerField(blank=True, null=True)
    fld_28 = models.IntegerField(blank=True, null=True)
    fld_29 = models.IntegerField(blank=True, null=True)
    fld_30 = models.IntegerField(blank=True, null=True)
    fld_31 = models.IntegerField(blank=True, null=True)
    fld_32 = models.IntegerField(blank=True, null=True)
    fld_33 = models.IntegerField(blank=True, null=True)
    fld_34 = models.IntegerField(blank=True, null=True)
    fld_35 = models.IntegerField(blank=True, null=True)
    fld_36 = models.IntegerField(blank=True, null=True)
    fld_37 = models.IntegerField(blank=True, null=True)
    fld_38 = models.IntegerField(blank=True, null=True)
    fld_39 = models.IntegerField(blank=True, null=True)
    fld_40 = models.IntegerField(blank=True, null=True)
    fld_41 = models.IntegerField(blank=True, null=True)
    fld_42 = models.IntegerField(blank=True, null=True)
    fld_43 = models.CharField(max_length=255, blank=True, null=True)
    fld_44 = models.CharField(max_length=255, blank=True, null=True)
    fld_45 = models.BigIntegerField(blank=True, null=True)
    fld_46 = models.FloatField(blank=True, null=True)
    fld_47 = models.CharField(max_length=255, blank=True, null=True)
    fld_48 = models.IntegerField(blank=True, null=True)
    fld_49 = models.CharField(max_length=255, blank=True, null=True)
    fld_50 = models.IntegerField(blank=True, null=True)
    fld_51 = models.IntegerField(blank=True, null=True)
    fld_52 = models.IntegerField(blank=True, null=True)
    fld_53 = models.IntegerField(blank=True, null=True)
    fld_54 = models.FloatField(blank=True, null=True)
    fld_55 = models.IntegerField(blank=True, null=True)
    fld_56 = models.IntegerField(blank=True, null=True)
    fld_57 = models.CharField(max_length=255, blank=True, null=True)
    fld_58 = models.TextField(blank=True, null=True)
    fld_60 = models.BigIntegerField(blank=True, null=True)
    fld_61 = models.IntegerField(blank=True, null=True)
    fld_62 = models.TextField(blank=True, null=True)
    fld_64 = models.IntegerField(blank=True, null=True)
    fld_65 = models.IntegerField(blank=True, null=True)
    fld_66 = models.IntegerField(blank=True, null=True)
    fld_67 = models.IntegerField(blank=True, null=True)
    fld_68 = models.FloatField(blank=True, null=True)
    fld_69 = models.CharField(max_length=255, blank=True, null=True)
    fld_70 = models.FloatField(blank=True, null=True)
    fld_71 = models.CharField(max_length=255, blank=True, null=True)
    fld_73 = models.CharField(max_length=255, blank=True, null=True)
    fld_74 = models.CharField(max_length=255, blank=True, null=True)
    fld_75 = models.CharField(max_length=255, blank=True, null=True)
    fld_76 = models.FloatField(blank=True, null=True)
    fld_77 = models.IntegerField(blank=True, null=True)
    fld_78 = models.IntegerField(blank=True, null=True)
    fld_79 = models.IntegerField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'detailsvalues'


class Comment(models.Model):
    id = models.AutoField(db_column='idComentario', primary_key=True)
    realty = models.ForeignKey('Realty', db_column='idPropiedad', max_length=30)
    content = models.TextField(db_column='contenido')
    ilike = models.TextField(db_column='megusta')
    sequence = models.CharField(db_column='secuencia', max_length=255)
    comment_type = models.IntegerField(db_column='tipo')
    creation_date = models.BigIntegerField(db_column='fechaIngreso')
    is_active = models.IntegerField(db_column='activado')
    in_trash = models.IntegerField(db_column='papelera')
    checked = models.IntegerField(db_column='revisado')

    class Meta:
        managed = False
        db_table = 'comentarios'
        verbose_name = 'Comentario'
        verbose_name_plural = 'Comentarios'


class Resources(models.Model):
    id = models.BigIntegerField(primary_key=True)
    realty = models.ForeignKey('Realty', db_column='idpropiedad', max_length=30,
                               verbose_name='Propiedad')
    description = models.CharField(max_length=255, verbose_name='Descipción')
    main = models.IntegerField()
    name = models.CharField(db_column='_name', max_length=255)
    mime = models.CharField(max_length=20)
    size = models.CharField(max_length=60)
    field_date = models.BigIntegerField(db_column='_date')
    field_hash = models.CharField(db_column='_hash', max_length=50)
    base = models.CharField(max_length=255)
    estado = models.SmallIntegerField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'resources'
        verbose_name = 'Recurso'
        verbose_name_plural = 'Recursos'

    def __unicode__(self):
        return u'%s' % self.name


# Comunication
class Message(models.Model):
    id = models.BigIntegerField(db_column='pkid', primary_key=True)
    messageid = models.CharField(db_column='id', max_length=50)
    realty = models.ForeignKey('Realty', db_column='idpropiedad', max_length=30)
    realtor = models.ForeignKey('Realtor', db_column='idcorredor', max_length=30)
    email = models.CharField(max_length=255)
    name = models.CharField(max_length=255, blank=True, null=True)
    telephone = models.CharField(max_length=255, blank=True, null=True)
    country = models.ForeignKey('Location', db_column='country')
    creation_date = models.BigIntegerField(db_column='_date')
    send_date = models.BigIntegerField(db_column='datetosend')
    expired = models.IntegerField(db_column='caducado')
    expired_bv = models.IntegerField(db_column='caducadobv')
    expiration_date = models.BigIntegerField(db_column='datecaducado', blank=True, null=True)
    archived = models.IntegerField(db_column='_save')
    answered = models.IntegerField(db_column='_respond')
    answer_date = models.BigIntegerField(db_column='daterespond')
    serialize = models.TextField()
    content = models.TextField()
    delete_record = models.IntegerField(db_column='rowdelete', blank=True, null=True)
    answer_su = models.IntegerField(db_column='respondsu', blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'messages'
        verbose_name = 'Mensaje'
        verbose_name_plural = 'Mensajes'


class Messages2(models.Model):
    id = models.BigIntegerField(db_column='pkid', primary_key=True)
    realty = models.ForeignKey('Realty', db_column='idpropiedad', max_length=30, blank=True,
                               null=True)
    email = models.CharField(max_length=255, blank=True, null=True)
    creation_date = models.DateTimeField(db_column='_date', blank=True, null=True)
    answer = models.CharField(db_column='respuesta', max_length=18, blank=True, null=True)
    send = models.IntegerField(blank=True, null=True)
    answer_date = models.DateTimeField(db_column='daterespuesta', blank=True, null=True)
    code = models.CharField(db_column='codechar', max_length=50, blank=True, null=True)
    nsend = models.IntegerField(blank=True, null=True)
    send_date = models.DateTimeField(db_column='_datesend', blank=True, null=True)
    end_b = models.IntegerField(db_column='bfin', blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'messages2'


class Notification(models.Model):
    notification_type = models.IntegerField(db_column='_type', blank=True, null=True)
    content = models.TextField(blank=True, null=True)
    notification_time = models.IntegerField(db_column='_time', blank=True, null=True)
    notification_status = models.IntegerField(db_column='_status', blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'notify'
        verbose_name = 'Notificación'
        verbose_name_plural = 'Notificaciones'


# Search and Sort
class Suggestion(models.Model):
    id = models.CharField(primary_key=True, max_length=50)
    text = models.CharField(db_column='texto', max_length=255, blank=True, null=True)
    creation_date = models.BigIntegerField(db_column='fecha', blank=True, null=True)
    score_0 = models.SmallIntegerField(db_column='score', blank=True, null=True)
    score_1 = models.SmallIntegerField(db_column='score0', blank=True, null=True)
    score_2 = models.SmallIntegerField(db_column='score1', blank=True, null=True)
    status = models.IntegerField(blank=True, null=True)
    admin = models.IntegerField(blank=True, null=True)
    grpid = models.SmallIntegerField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'suggestions'
        verbose_name = 'Sugerencia de Búsqueda'
        verbose_name_plural = 'Sugerencias de Búsqueda'

    def __unicode__(self):
        return u'%s' % self.text


class SuggestionsStats(models.Model):
    sugestion = models.ForeignKey('Suggestion', db_column='Id', max_length=50)
    text = models.CharField(db_column='texto', max_length=255, blank=True, null=True)
    creation_date = models.BigIntegerField(db_column='fecha', blank=True, null=True)
    score_1 = models.SmallIntegerField(db_column='score0', blank=True, null=True)
    score_2 = models.SmallIntegerField(db_column='score1', blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'stsuggestions'

# =============================================================================
# Advertising
# =============================================================================


class Zone(models.Model):
    location = models.ForeignKey('Location', db_column='Id')
    location_slug = models.CharField(db_column='slocation', max_length=255)
    location_name = models.CharField(db_column='Location', max_length=255, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'zones'
        unique_together = (('location', 'location_slug'),)


class Banner(models.Model):
    id = models.BigIntegerField(db_column='Idbaner', primary_key=True)
    realtor = models.ForeignKey('Realtor', db_column='Idcorredor', max_length=20, blank=True,
                                null=True)
    banner_type = models.CharField(db_column='Tipo', max_length=1, blank=True, null=True)
    zone = models.ForeignKey('BannerZone', db_column='Idzona', blank=True, null=True)
    content = models.TextField(db_column='Content', blank=True, null=True)
    is_active = models.CharField(db_column='Activate', max_length=1, blank=True, null=True)
    impressions = models.BigIntegerField(db_column='Counter', blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'baners'
        verbose_name = 'Baner'
        verbose_name_plural = 'Baners'

    def __unicode__(self):
        return u'%s %s' % (self.realtor.name, self.zone.name)


class BannerZone(models.Model):
    id = models.BigIntegerField(db_column='Idzona', primary_key=True)
    location = models.ForeignKey('Location', db_column='Idgeo', blank=True, null=True)
    name = models.CharField(db_column='Nombre', max_length=255, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'banerzonas'
        verbose_name = 'Zona de Baner'
        verbose_name_plural = 'Zonas de Baner'

    def __unicode__(self):
        return u'%s' % self.name


class Sponsorship(models.Model):
    realty = models.OneToOneField('Realty', db_column='Idpropiedad', primary_key=True,
                                  max_length=30)
    geo = models.CharField(db_column='Geo', max_length=255, blank=True, null=True)
    location = models.CharField(db_column='Location', max_length=255, blank=True, null=True)
    locname = models.CharField(db_column='LocName', max_length=255, blank=True, null=True)
    category = models.ForeignKey('Category', db_column='Categoria', blank=True, null=True)
    content = models.TextField(db_column='Content', blank=True, null=True)
    rental_price = models.FloatField(db_column='Palquiler', blank=True, null=True)
    sale_price = models.FloatField(db_column='Pventa', blank=True, null=True)
    is_active = models.CharField(db_column='Activado', max_length=1)
    send = models.IntegerField(db_column='Send', blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'destacados'
        verbose_name = 'Destacado'
        verbose_name_plural = 'Destacados'

    def __unicode__(self):
        return u'%s' % self.realty.name


class SponsorshipCountry(models.Model):
    id = models.CharField(db_column='pkid', primary_key=True, max_length=3)
    name = models.CharField(db_column='sname', max_length=255, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'destacadosCountries'

    def __unicode__(self):
        return u'%s' % self.name


class SponsorshipOrder(models.Model):
    id = models.BigIntegerField(db_column='idorder', primary_key=True)
    realty = models.ForeignKey('Realty', db_column='idpropiedad', max_length=30, blank=True,
                               null=True)
    currency = models.CharField(db_column='coin', max_length=3, blank=True, null=True)
    price = models.FloatField(blank=True, null=True)
    creation_date = models.BigIntegerField(db_column='_date', blank=True, null=True)
    sponsored_date = models.BigIntegerField(db_column='datesponsored', blank=True, null=True)
    name = models.CharField(max_length=255, blank=True, null=True)
    email = models.CharField(max_length=255, blank=True, null=True)
    status = models.CharField(max_length=30, blank=True, null=True)
    transactionid = models.CharField(db_column='payid', max_length=60, blank=True, null=True)
    is_sponsored = models.IntegerField(db_column='sponsored', blank=True, null=True)
    end_sponsored = models.IntegerField(db_column='endsponsored', blank=True, null=True)
    payment_method = models.CharField(db_column='typepay', max_length=44, blank=True, null=True)
    reactivation_date = models.BigIntegerField(db_column='secondtime', blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'destacadosOrder'


class SponsorshipSort(models.Model):
    id = models.CharField(db_column='Idsort', primary_key=True, max_length=44)
    serialize = models.TextField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'destacadosSort'


class SponsorshipStatCountry(models.Model):
    """
    FIXME: Use Entity-Attribute-Value storage model
    """
    realty = models.CharField(db_column='idpropiedad', max_length=30)
    yearmonth = models.CharField(max_length=7)
    campocrv = models.BigIntegerField()
    campocri = models.BigIntegerField()
    campouknv = models.BigIntegerField()
    campoukni = models.BigIntegerField()
    campovev = models.BigIntegerField()
    campovei = models.BigIntegerField()
    campousv = models.BigIntegerField()
    campousi = models.BigIntegerField()
    campogbv = models.BigIntegerField()
    campogbi = models.BigIntegerField()
    campocav = models.BigIntegerField()
    campocai = models.BigIntegerField()
    campoesv = models.BigIntegerField()
    campoesi = models.BigIntegerField()
    campogtv = models.BigIntegerField()
    campogti = models.BigIntegerField()
    campopav = models.BigIntegerField()
    campopai = models.BigIntegerField()
    campochv = models.BigIntegerField()
    campochi = models.BigIntegerField()
    campofrv = models.BigIntegerField()
    campofri = models.BigIntegerField()
    campohnv = models.BigIntegerField()
    campohni = models.BigIntegerField()
    campopev = models.BigIntegerField()
    campopei = models.BigIntegerField()
    campomxv = models.BigIntegerField()
    campomxi = models.BigIntegerField()
    campocnv = models.BigIntegerField()
    campocni = models.BigIntegerField()
    campohkv = models.BigIntegerField()
    campohki = models.BigIntegerField()
    camponiv = models.BigIntegerField()
    camponii = models.BigIntegerField()
    camposvv = models.BigIntegerField()
    camposvi = models.BigIntegerField()
    camporuv = models.BigIntegerField()
    camporui = models.BigIntegerField()
    campocov = models.BigIntegerField()
    campocoi = models.BigIntegerField()
    campodev = models.BigIntegerField()
    campodei = models.BigIntegerField()
    campokwv = models.BigIntegerField()
    campokwi = models.BigIntegerField()
    campotwv = models.BigIntegerField()
    campotwi = models.BigIntegerField()
    campoecv = models.BigIntegerField()
    campoeci = models.BigIntegerField()
    camponlv = models.BigIntegerField()
    camponli = models.BigIntegerField()
    campodov = models.BigIntegerField()
    campodoi = models.BigIntegerField()
    camponov = models.BigIntegerField()
    camponoi = models.BigIntegerField()
    campouav = models.BigIntegerField()
    campouai = models.BigIntegerField()
    campoauv = models.BigIntegerField()
    campoaui = models.BigIntegerField()
    campoarv = models.BigIntegerField()
    campoari = models.BigIntegerField()
    campoitv = models.BigIntegerField()
    campoiti = models.BigIntegerField()
    campobev = models.BigIntegerField()
    campobei = models.BigIntegerField()
    campovnv = models.BigIntegerField()
    campovni = models.BigIntegerField()
    campodkv = models.BigIntegerField()
    campodki = models.BigIntegerField()
    camposev = models.BigIntegerField()
    camposei = models.BigIntegerField()
    camporov = models.BigIntegerField()
    camporoi = models.BigIntegerField()
    camposgv = models.BigIntegerField()
    camposgi = models.BigIntegerField()
    campohuv = models.BigIntegerField()
    campohui = models.BigIntegerField()
    campokrv = models.BigIntegerField()
    campokri = models.BigIntegerField()
    campoclv = models.BigIntegerField()
    campocli = models.BigIntegerField()
    campopyv = models.BigIntegerField()
    campopyi = models.BigIntegerField()
    campophv = models.BigIntegerField()
    campophi = models.BigIntegerField()
    campoptv = models.BigIntegerField()
    campopti = models.BigIntegerField()
    campoeuv = models.BigIntegerField()
    campoeui = models.BigIntegerField()
    campobrv = models.BigIntegerField()
    campobri = models.BigIntegerField()
    campogpv = models.BigIntegerField()
    campogpi = models.BigIntegerField()
    campoatv = models.BigIntegerField()
    campoati = models.BigIntegerField()
    camposnv = models.BigIntegerField()
    camposni = models.BigIntegerField()
    campoilv = models.BigIntegerField()
    campoili = models.BigIntegerField()
    campoplv = models.BigIntegerField()
    campopli = models.BigIntegerField()
    campoawv = models.BigIntegerField()
    campoawi = models.BigIntegerField()
    campoprv = models.BigIntegerField()
    campopri = models.BigIntegerField()
    campoczv = models.BigIntegerField()
    campoczi = models.BigIntegerField()
    camposav = models.BigIntegerField()
    camposai = models.BigIntegerField()
    campobov = models.BigIntegerField()
    campoboi = models.BigIntegerField()
    campojpv = models.BigIntegerField()
    campojpi = models.BigIntegerField()
    campobzv = models.BigIntegerField()
    campobzi = models.BigIntegerField()
    camponzv = models.BigIntegerField()
    camponzi = models.BigIntegerField()
    campomuv = models.BigIntegerField()
    campomui = models.BigIntegerField()
    campociv = models.BigIntegerField()
    campocii = models.BigIntegerField()
    campoa2v = models.BigIntegerField()
    campoa2i = models.BigIntegerField()
    campoqav = models.BigIntegerField()
    campoqai = models.BigIntegerField()
    camposkv = models.BigIntegerField()
    camposki = models.BigIntegerField()
    campongv = models.BigIntegerField()
    campongi = models.BigIntegerField()
    campottv = models.BigIntegerField()
    campotti = models.BigIntegerField()
    campoaev = models.BigIntegerField()
    campoaei = models.BigIntegerField()
    campouyv = models.BigIntegerField()
    campouyi = models.BigIntegerField()
    campobgv = models.BigIntegerField()
    campobgi = models.BigIntegerField()
    campothv = models.BigIntegerField()
    campothi = models.BigIntegerField()
    campobwv = models.BigIntegerField()
    campobwi = models.BigIntegerField()
    campolvv = models.BigIntegerField()
    campolvi = models.BigIntegerField()
    campogrv = models.BigIntegerField()
    campogri = models.BigIntegerField()
    campotrv = models.BigIntegerField()
    campotri = models.BigIntegerField()
    campobav = models.BigIntegerField()
    campobai = models.BigIntegerField()
    campomav = models.BigIntegerField()
    campomai = models.BigIntegerField()
    campoghv = models.BigIntegerField()
    campoghi = models.BigIntegerField()
    campoafv = models.BigIntegerField()
    campoafi = models.BigIntegerField()
    campocuv = models.BigIntegerField()
    campocui = models.BigIntegerField()
    campoinv = models.BigIntegerField()
    campoini = models.BigIntegerField()
    campopfv = models.BigIntegerField()
    campopfi = models.BigIntegerField()
    campoaiv = models.BigIntegerField()
    campoaii = models.BigIntegerField()
    campobmv = models.BigIntegerField()
    campobmi = models.BigIntegerField()
    campomyv = models.BigIntegerField()
    campomyi = models.BigIntegerField()
    campoidv = models.BigIntegerField()
    campoidi = models.BigIntegerField()
    campotnv = models.BigIntegerField()
    campotni = models.BigIntegerField()
    campocmv = models.BigIntegerField()
    campocmi = models.BigIntegerField()
    camporsv = models.BigIntegerField()
    camporsi = models.BigIntegerField()
    campoaov = models.BigIntegerField()
    campoaoi = models.BigIntegerField()
    campofiv = models.BigIntegerField()
    campofii = models.BigIntegerField()
    campoadv = models.BigIntegerField()
    campoadi = models.BigIntegerField()
    campoluv = models.BigIntegerField()
    campolui = models.BigIntegerField()
    campokzv = models.BigIntegerField()
    campokzi = models.BigIntegerField()
    campodzv = models.BigIntegerField()
    campodzi = models.BigIntegerField()
    campoiev = models.BigIntegerField()
    campoiei = models.BigIntegerField()
    camposrv = models.BigIntegerField()
    camposri = models.BigIntegerField()
    campogev = models.BigIntegerField()
    campogei = models.BigIntegerField()
    camposiv = models.BigIntegerField()
    camposii = models.BigIntegerField()
    campoanv = models.BigIntegerField()
    campoani = models.BigIntegerField()
    campomzv = models.BigIntegerField()
    campomzi = models.BigIntegerField()
    campokhv = models.BigIntegerField()
    campokhi = models.BigIntegerField()
    campobdv = models.BigIntegerField()
    campobdi = models.BigIntegerField()
    campohtv = models.BigIntegerField()
    campohti = models.BigIntegerField()
    campoirv = models.BigIntegerField()
    campoiri = models.BigIntegerField()
    campokyv = models.BigIntegerField()
    campokyi = models.BigIntegerField()
    campobyv = models.BigIntegerField()
    campobyi = models.BigIntegerField()
    campoeev = models.BigIntegerField()
    campoeei = models.BigIntegerField()
    campozav = models.BigIntegerField()
    campozai = models.BigIntegerField()
    campohrv = models.BigIntegerField()
    campohri = models.BigIntegerField()
    campoyev = models.BigIntegerField()
    campoyei = models.BigIntegerField()
    campoltv = models.BigIntegerField()
    campolti = models.BigIntegerField()
    campojmv = models.BigIntegerField()
    campojmi = models.BigIntegerField()
    campomgv = models.BigIntegerField()
    campomgi = models.BigIntegerField()
    campoa1v = models.BigIntegerField()
    campoa1i = models.BigIntegerField()
    campomqv = models.BigIntegerField()
    campomqi = models.BigIntegerField()
    campogqv = models.BigIntegerField()
    campogqi = models.BigIntegerField()
    campomkv = models.BigIntegerField()
    campomki = models.BigIntegerField()
    campocyv = models.BigIntegerField()
    campocyi = models.BigIntegerField()
    campomcv = models.BigIntegerField()
    campomci = models.BigIntegerField()
    campotzv = models.BigIntegerField()
    campotzi = models.BigIntegerField()
    camporev = models.BigIntegerField()
    camporei = models.BigIntegerField()
    campobfv = models.BigIntegerField()
    campobfi = models.BigIntegerField()
    campoapv = models.BigIntegerField()
    campoapi = models.BigIntegerField()
    campoegv = models.BigIntegerField()
    campoegi = models.BigIntegerField()
    campomdv = models.BigIntegerField()
    campomdi = models.BigIntegerField()
    campoisv = models.BigIntegerField()
    campoisi = models.BigIntegerField()
    campobhv = models.BigIntegerField()
    campobhi = models.BigIntegerField()
    campoazv = models.BigIntegerField()
    campoazi = models.BigIntegerField()
    camponpv = models.BigIntegerField()
    camponpi = models.BigIntegerField()
    campokgv = models.BigIntegerField()
    campokgi = models.BigIntegerField()
    campodjv = models.BigIntegerField()
    campodji = models.BigIntegerField()
    campoamv = models.BigIntegerField()
    campoami = models.BigIntegerField()
    camponcv = models.BigIntegerField()
    camponci = models.BigIntegerField()
    campoiqv = models.BigIntegerField()
    campoiqi = models.BigIntegerField()
    campokev = models.BigIntegerField()
    campokei = models.BigIntegerField()
    campobbv = models.BigIntegerField()
    campobbi = models.BigIntegerField()
    campotcv = models.BigIntegerField()
    campotci = models.BigIntegerField()
    campouzv = models.BigIntegerField()
    campouzi = models.BigIntegerField()
    campobsv = models.BigIntegerField()
    campobsi = models.BigIntegerField()
    campoomv = models.BigIntegerField()
    campoomi = models.BigIntegerField()
    campopkv = models.BigIntegerField()
    campopki = models.BigIntegerField()
    campojov = models.BigIntegerField()
    campojoi = models.BigIntegerField()
    campolbv = models.BigIntegerField()
    campolbi = models.BigIntegerField()
    campoagv = models.BigIntegerField()
    campoagi = models.BigIntegerField()
    campogyv = models.BigIntegerField()
    campogyi = models.BigIntegerField()
    campovcv = models.BigIntegerField()
    campovci = models.BigIntegerField()
    camposyv = models.BigIntegerField()
    camposyi = models.BigIntegerField()
    campobnv = models.BigIntegerField()
    campobni = models.BigIntegerField()
    campopsv = models.BigIntegerField()
    campopsi = models.BigIntegerField()
    campomvv = models.BigIntegerField()
    campomvi = models.BigIntegerField()
    campomov = models.BigIntegerField()
    campomoi = models.BigIntegerField()
    campoalv = models.BigIntegerField()
    campoali = models.BigIntegerField()
    campolyv = models.BigIntegerField()
    campolyi = models.BigIntegerField()
    campoytv = models.BigIntegerField()
    campoyti = models.BigIntegerField()
    campomtv = models.BigIntegerField()
    campomti = models.BigIntegerField()
    campogfv = models.BigIntegerField()
    campogfi = models.BigIntegerField()
    campoviv = models.BigIntegerField()
    campovii = models.BigIntegerField()
    campolav = models.BigIntegerField()
    campolai = models.BigIntegerField()
    camposcv = models.BigIntegerField()
    camposci = models.BigIntegerField()
    campozmv = models.BigIntegerField()
    campozmi = models.BigIntegerField()
    campoggv = models.BigIntegerField()
    campoggi = models.BigIntegerField()
    campobjv = models.BigIntegerField()
    campobji = models.BigIntegerField()
    campogiv = models.BigIntegerField()
    campogii = models.BigIntegerField()
    campoetv = models.BigIntegerField()
    campoeti = models.BigIntegerField()
    campognv = models.BigIntegerField()
    campogni = models.BigIntegerField()
    campommv = models.BigIntegerField()
    campommi = models.BigIntegerField()
    campocvv = models.BigIntegerField()
    campocvi = models.BigIntegerField()
    campoliv = models.BigIntegerField()
    campolii = models.BigIntegerField()
    campomfv = models.BigIntegerField()
    campomfi = models.BigIntegerField()
    campogav = models.BigIntegerField()
    campogai = models.BigIntegerField()
    campostv = models.BigIntegerField()
    camposti = models.BigIntegerField()
    campolrv = models.BigIntegerField()
    campolri = models.BigIntegerField()
    campojev = models.BigIntegerField()
    campojei = models.BigIntegerField()
    campoknv = models.BigIntegerField()
    campokni = models.BigIntegerField()
    campogmv = models.BigIntegerField()
    campogmi = models.BigIntegerField()
    campogdv = models.BigIntegerField()
    campogdi = models.BigIntegerField()
    campolkv = models.BigIntegerField()
    campolki = models.BigIntegerField()
    campoguv = models.BigIntegerField()
    campogui = models.BigIntegerField()
    campozwv = models.BigIntegerField()
    campozwi = models.BigIntegerField()
    campotlv = models.BigIntegerField()
    campotli = models.BigIntegerField()
    campocgv = models.BigIntegerField()
    campocgi = models.BigIntegerField()
    campovav = models.BigIntegerField()
    campovai = models.BigIntegerField()
    campomrv = models.BigIntegerField()
    campomri = models.BigIntegerField()
    campotgv = models.BigIntegerField()
    campotgi = models.BigIntegerField()
    campolcv = models.BigIntegerField()
    campolci = models.BigIntegerField()

    class Meta:
        managed = False
        db_table = 'destacadosStatisticcountries'
        unique_together = (('realty', 'yearmonth'),)


# FIXME: Use Entity-Attribute-Value storage model
class SponsorshipStatDay(models.Model):
    realty = models.CharField(db_column='idpropiedad', max_length=30)
    yearmonth = models.CharField(max_length=7)
    dayv01 = models.BigIntegerField(blank=True, null=True)
    dayi01 = models.BigIntegerField(blank=True, null=True)
    dayv02 = models.BigIntegerField(blank=True, null=True)
    dayi02 = models.BigIntegerField(blank=True, null=True)
    dayv03 = models.BigIntegerField(blank=True, null=True)
    dayi03 = models.BigIntegerField(blank=True, null=True)
    dayv04 = models.BigIntegerField(blank=True, null=True)
    dayi04 = models.BigIntegerField(blank=True, null=True)
    dayv05 = models.BigIntegerField(blank=True, null=True)
    dayi05 = models.BigIntegerField(blank=True, null=True)
    dayv06 = models.BigIntegerField(blank=True, null=True)
    dayi06 = models.BigIntegerField(blank=True, null=True)
    dayv07 = models.BigIntegerField(blank=True, null=True)
    dayi07 = models.BigIntegerField(blank=True, null=True)
    dayv08 = models.BigIntegerField(blank=True, null=True)
    dayi08 = models.BigIntegerField(blank=True, null=True)
    dayv09 = models.BigIntegerField(blank=True, null=True)
    dayi09 = models.BigIntegerField(blank=True, null=True)
    dayv10 = models.BigIntegerField(blank=True, null=True)
    dayi10 = models.BigIntegerField(blank=True, null=True)
    dayv11 = models.BigIntegerField(blank=True, null=True)
    dayi11 = models.BigIntegerField(blank=True, null=True)
    dayv12 = models.BigIntegerField(blank=True, null=True)
    dayi12 = models.BigIntegerField(blank=True, null=True)
    dayv13 = models.BigIntegerField(blank=True, null=True)
    dayi13 = models.BigIntegerField(blank=True, null=True)
    dayv14 = models.BigIntegerField(blank=True, null=True)
    dayi14 = models.BigIntegerField(blank=True, null=True)
    dayv15 = models.BigIntegerField(blank=True, null=True)
    dayi15 = models.BigIntegerField(blank=True, null=True)
    dayv16 = models.BigIntegerField(blank=True, null=True)
    dayi16 = models.BigIntegerField(blank=True, null=True)
    dayv17 = models.BigIntegerField(blank=True, null=True)
    dayi17 = models.BigIntegerField(blank=True, null=True)
    dayv18 = models.BigIntegerField(blank=True, null=True)
    dayi18 = models.BigIntegerField(blank=True, null=True)
    dayv19 = models.BigIntegerField(blank=True, null=True)
    dayi19 = models.BigIntegerField(blank=True, null=True)
    dayv20 = models.BigIntegerField(blank=True, null=True)
    dayi20 = models.BigIntegerField(blank=True, null=True)
    dayv21 = models.BigIntegerField(blank=True, null=True)
    dayi21 = models.BigIntegerField(blank=True, null=True)
    dayv22 = models.BigIntegerField(blank=True, null=True)
    dayi22 = models.BigIntegerField(blank=True, null=True)
    dayv23 = models.BigIntegerField(blank=True, null=True)
    dayi23 = models.BigIntegerField(blank=True, null=True)
    dayv24 = models.BigIntegerField(blank=True, null=True)
    dayi24 = models.BigIntegerField(blank=True, null=True)
    dayv25 = models.BigIntegerField(blank=True, null=True)
    dayi25 = models.BigIntegerField(blank=True, null=True)
    dayv26 = models.BigIntegerField(blank=True, null=True)
    dayi26 = models.BigIntegerField(blank=True, null=True)
    dayv27 = models.BigIntegerField(blank=True, null=True)
    dayi27 = models.BigIntegerField(blank=True, null=True)
    dayv28 = models.BigIntegerField(blank=True, null=True)
    dayi28 = models.BigIntegerField(blank=True, null=True)
    dayv29 = models.BigIntegerField(blank=True, null=True)
    dayi29 = models.BigIntegerField(blank=True, null=True)
    dayv30 = models.BigIntegerField(blank=True, null=True)
    dayi30 = models.BigIntegerField(blank=True, null=True)
    dayv31 = models.BigIntegerField(blank=True, null=True)
    dayi31 = models.BigIntegerField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'destacadosStatisticdays'
        unique_together = (('realty', 'yearmonth'),)


# FIXME: Use Entity-Attribute-Value storage model
class SponsorshipStatHour(models.Model):
    realty = models.CharField(db_column='idpropiedad', max_length=30)
    yearmonth = models.CharField(max_length=7)
    hourv00 = models.BigIntegerField(blank=True, null=True)
    houri00 = models.BigIntegerField(blank=True, null=True)
    hourv01 = models.BigIntegerField(blank=True, null=True)
    houri01 = models.BigIntegerField(blank=True, null=True)
    hourv02 = models.BigIntegerField(blank=True, null=True)
    houri02 = models.BigIntegerField(blank=True, null=True)
    hourv03 = models.BigIntegerField(blank=True, null=True)
    houri03 = models.BigIntegerField(blank=True, null=True)
    hourv04 = models.BigIntegerField(blank=True, null=True)
    houri04 = models.BigIntegerField(blank=True, null=True)
    hourv05 = models.BigIntegerField(blank=True, null=True)
    houri05 = models.BigIntegerField(blank=True, null=True)
    hourv06 = models.BigIntegerField(blank=True, null=True)
    houri06 = models.BigIntegerField(blank=True, null=True)
    hourv07 = models.BigIntegerField(blank=True, null=True)
    houri07 = models.BigIntegerField(blank=True, null=True)
    hourv08 = models.BigIntegerField(blank=True, null=True)
    houri08 = models.BigIntegerField(blank=True, null=True)
    hourv09 = models.BigIntegerField(blank=True, null=True)
    houri09 = models.BigIntegerField(blank=True, null=True)
    hourv10 = models.BigIntegerField(blank=True, null=True)
    houri10 = models.BigIntegerField(blank=True, null=True)
    hourv11 = models.BigIntegerField(blank=True, null=True)
    houri11 = models.BigIntegerField(blank=True, null=True)
    hourv12 = models.BigIntegerField(blank=True, null=True)
    houri12 = models.BigIntegerField(blank=True, null=True)
    hourv13 = models.BigIntegerField(blank=True, null=True)
    houri13 = models.BigIntegerField(blank=True, null=True)
    hourv14 = models.BigIntegerField(blank=True, null=True)
    houri14 = models.BigIntegerField(blank=True, null=True)
    hourv15 = models.BigIntegerField(blank=True, null=True)
    houri15 = models.BigIntegerField(blank=True, null=True)
    hourv16 = models.BigIntegerField(blank=True, null=True)
    houri16 = models.BigIntegerField(blank=True, null=True)
    hourv17 = models.BigIntegerField(blank=True, null=True)
    houri17 = models.BigIntegerField(blank=True, null=True)
    hourv18 = models.BigIntegerField(blank=True, null=True)
    houri18 = models.BigIntegerField(blank=True, null=True)
    hourv19 = models.BigIntegerField(blank=True, null=True)
    houri19 = models.BigIntegerField(blank=True, null=True)
    hourv20 = models.BigIntegerField(blank=True, null=True)
    houri20 = models.BigIntegerField(blank=True, null=True)
    hourv21 = models.BigIntegerField(blank=True, null=True)
    houri21 = models.BigIntegerField(blank=True, null=True)
    hourv22 = models.BigIntegerField(blank=True, null=True)
    houri22 = models.BigIntegerField(blank=True, null=True)
    hourv23 = models.BigIntegerField(blank=True, null=True)
    houri23 = models.BigIntegerField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'destacadosStatistichours'
        unique_together = (('realty', 'yearmonth'),)


class SponsorshipDemo(models.Model):
    realty = models.OneToOneField('Realty', db_column='Idpropiedad', primary_key=True,
                                  max_length=30)
    geo = models.CharField(db_column='Geo', max_length=255, blank=True, null=True)
    location = models.CharField(db_column='Location', max_length=255, blank=True, null=True)
    locname = models.CharField(db_column='LocName', max_length=255, blank=True, null=True)
    category = models.ForeignKey('Category', db_column='Categoria', blank=True, null=True)
    content = models.TextField(db_column='Content', blank=True, null=True)
    rental_price = models.FloatField(db_column='Palquiler', blank=True, null=True)
    sale_price = models.FloatField(db_column='Pventa', blank=True, null=True)
    is_active = models.CharField(db_column='Activado', max_length=1)
    send = models.IntegerField(db_column='Send', blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'destacadosdemo'


# =============================================================================
# Stats
# =============================================================================
class UserAccess(models.Model):
    realtor = models.OneToOneField('Realtor', db_column='Idcorredor', primary_key=True, max_length=20)
    last_login = models.IntegerField(db_column='lasttime', blank=True, null=True)
    ip = models.CharField(max_length=20, blank=True, null=True)
    ips = models.TextField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'uaccess'


class Statistics(models.Model):
    ads_code = models.CharField(db_column='CodeAds', max_length=30)
    country = models.CharField(db_column='Country', max_length=100)
    prints = models.BigIntegerField(db_column='Prints', blank=True, null=True)
    views = models.BigIntegerField(db_column='Views', blank=True, null=True)
    day = models.IntegerField(db_column='Day', blank=True, null=True)
    month = models.IntegerField(db_column='Month', blank=True, null=True)
    year = models.IntegerField(db_column='Year', blank=True, null=True)
    hours = models.IntegerField(db_column='Hours', blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'statistics'
        unique_together = (('ads_code', 'country'),)

# FIXME: use a primary key instead of compose key
"""
class MostWanted(models.Model):
    OPERATION_CHOICES = ((0, 'Renta'), (1, 'Venta'))
    location = models.ForeignKey('Location', db_column='Idlocation')
    location_name = models.CharField(db_column='location', max_length=255)
    location_slug = models.CharField(db_column='slocation', max_length=255, blank=True, null=True)
    sequence = models.CharField(db_column='secuencia', max_length=255)
    category = models.ForeignKey('Category', db_column='idcategoria')
    operation = models.IntegerField(db_column='operacion', choices=OPERATION_CHOICES)
    total = models.BigIntegerField()
    level = models.IntegerField(db_column='nivel')
    last_update = models.BigIntegerField(db_column='time')
    ranking = models.FloatField(db_column='rnk', blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'iconsii'
        unique_together = (('location', 'category', 'operation', 'location_name'),)
"""

# FIXME: use a primary key
"""
class Log(models.Model):
    fingerprint = models.CharField(max_length=50)
    logtime = models.BigIntegerField(blank=True, null=True)
    realtor = models.ForeignKey('Realtor', db_column='Idcorredor', max_length=20, blank=True,
                                null=True)
    realty = models.ForeignKey('Realty', db_column='Idpropiedad', max_length=30, blank=True,
                               null=True)
    type = models.CharField(max_length=1, blank=True, null=True)
    score = models.BigIntegerField(blank=True, null=True)
    opscore = models.BigIntegerField(blank=True, null=True)
    country = models.ForeignKey('Location', db_column='country', blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'logs'
"""


# XXX: Unknow fields meaning
# FIXME: use a primary key
"""
class LogRanking(models.Model):

    logtime = models.BigIntegerField()
    lasttime = models.BigIntegerField(blank=True, null=True)
    realtor = models.ForeignKey('Realtor', db_column='idcorredor', max_length=20, blank=True,
                                null=True)
    realty = models.ForeignKey('Realty', db_column='idpropiedad', max_length=30)
    scorev = models.BigIntegerField(blank=True, null=True)
    tv = models.BigIntegerField(blank=True, null=True)
    scorep = models.BigIntegerField(blank=True, null=True)
    tp = models.BigIntegerField(blank=True, null=True)
    scorer = models.BigIntegerField(blank=True, null=True)
    tr = models.BigIntegerField(blank=True, null=True)
    scoref = models.BigIntegerField(blank=True, null=True)
    tf = models.BigIntegerField(blank=True, null=True)
    scoret = models.BigIntegerField(blank=True, null=True)
    tt = models.BigIntegerField(blank=True, null=True)
    maxphotos = models.BigIntegerField(db_column='maxfotos', blank=True, null=True)
    opscore = models.IntegerField()
    country = models.ForeignKey('Location', db_column='country', blank=True, null=True)
    lockt = models.IntegerField(blank=True, null=True)
    havetp = models.BigIntegerField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'logsrnk'
        unique_together = (('logtime', 'realty', 'opscore'),)
"""


# =============================================================================
# Administration
# =============================================================================
# XXX: Usage Unknow
class Navigation(models.Model):
    id = models.CharField(primary_key=True, max_length=255)
    items = models.TextField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'navigation'


# XXX: Usage Unknow
class TableSetting(models.Model):
    vary = models.FloatField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'tblsettings'


class Email(models.Model):
    id = models.CharField(primary_key=True, max_length=32)
    email = models.EmailField(max_length=255)
    is_banned = models.CharField(db_column='baneado', max_length=1)
    last_update = models.BigIntegerField(db_column='time')
    sends = models.BigIntegerField()
    is_pending = models.IntegerField(db_column='pending', blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'emails'


# XXX: Split translation from multipe content generic relations
# XXX: Use GenericForeignKey, Abstract or Parent model
# XXX: Use Language File and Rosseta for Static Translation
# XXX: Use some tranlation app for model translations
class Dictionary(models.Model):
    id = models.BigIntegerField(db_column='Iddiccionario', primary_key=True)
    object_id = models.BigIntegerField(db_column='Idforaneo', blank=True, null=True)
    content_type = models.CharField(db_column='Referencia', max_length=255)
    language = models.CharField(db_column='Language', max_length=2)
    translation = models.CharField(db_column='Traduccion', max_length=255, blank=True, null=True)
    translation_alternative = models.CharField(db_column='TraduccionII', max_length=255, blank=True,
                                               null=True)
    path = models.CharField(db_column='Path', max_length=255, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'diccionario'
        verbose_name_plural = 'Diccionario'
        verbose_name = 'Item'


# XXX: Unknow utility
class IpCountry(models.Model):
    ip_from = models.IntegerField(blank=True, null=True)
    ip_to = models.IntegerField(blank=True, null=True)
    country_code2 = models.CharField(max_length=2, blank=True, null=True)
    country_code3 = models.CharField(max_length=3, blank=True, null=True)
    country_name = models.CharField(max_length=50, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'ipcountries'


class Plugin(models.Model):
    id = models.BigIntegerField(db_column='Idplugin', primary_key=True)
    name = models.CharField(db_column='Nombre', max_length=32, blank=True, null=True)
    description = models.CharField(db_column='Descripcion', max_length=255, blank=True, null=True)
    events = models.CharField(db_column='Events', max_length=255, blank=True, null=True)
    order = models.IntegerField(db_column='Order', blank=True, null=True)
    plugin_type = models.CharField(db_column='Type', max_length=32, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'plugins'

    def __unicode__(self):
        return u'%s' % self.name


class Register(models.Model):
    id = models.CharField(primary_key=True, max_length=255)
    email = models.CharField(max_length=255, blank=True, null=True)
    creation_date = models.IntegerField(db_column='_time', blank=True, null=True)
    status = models.IntegerField(db_column='_status', blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'register'


# =============================================================================
# Tools
# =============================================================================
class Converter(models.Model):
    id = models.AutoField(db_column='Sl_no', primary_key=True)
    exchange_rate = models.FloatField(db_column='Ex_rate', verbose_name='Exchange Rate')
    currency = models.ForeignKey('Currency', db_column='Idmoneda', verbose_name='Moneda')
    last_update = models.BigIntegerField(db_column='Time_update',
                                         verbose_name='última Actualización')

    class Meta:
        managed = False
        db_table = 'conversor'
        verbose_name = 'Tasa de Cambio'
        verbose_name_plural = 'Tasas de Cambio'
