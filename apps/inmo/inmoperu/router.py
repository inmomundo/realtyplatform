class InmoPeruRouter(object):
    def db_for_read(self, model, **hints):
        "Point all operations on inmoperu models to 'inmoperu'"
        if model._meta.app_label == 'inmoperu':
            return 'inmoperu'
        return 'default'

    def db_for_write(self, model, **hints):
        "Point all operations on inmoperu models to 'inmoperu'"
        if model._meta.app_label == 'inmoperu':
            return 'inmoperu'
        return 'default'

    def allow_relation(self, obj1, obj2, **hints):
        "Allow any relation if a both models in inmoperu app"
        if obj1._meta.app_label == 'inmoperu' and obj2._meta.app_label == 'inmoperu':
            return True
        # Allow if neither is inmoperu app
        elif 'inmoperu' not in [obj1._meta.app_label, obj2._meta.app_label]:
            return True
        return False

    def allow_migrate(self, db, model):
        if db == 'inmoperu' or model._meta.app_label == "inmoperu":
            return False  # we're not using syncdb on our legacy database
        else:  # but all other models/databases are fine
            return True
