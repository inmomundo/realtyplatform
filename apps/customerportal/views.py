from django.utils.translation import ugettext as _
from django.views.generic.base import TemplateView
from apps.classifieds.models import Category, Location, Ad
from django.views.generic.list import ListView
from django.views.generic import DetailView
from django.db import connections
from django.db.backends.sqlite3.base import DatabaseWrapper

from test import test_support

from django.http import request

class HomePageView(TemplateView):

    output = _("Welcome to my site.")
    template_name = "home.html"

    def get_context_data(self, **kwargs):
        context = super(HomePageView, self).get_context_data(**kwargs)
        context['metatitle'] = 'Bienes Raices en Peru'
        context['addthis_account'] = 'ra-51070c8464c9a004'
        context['mcategories'] = Category.objects.all()
        context['menucat_in_index'] = ((1, 0), (1, 1), (2, 0), (2, 1), (3, 0), (3, 1), (5, 0), (6, 0), (7, 0))
        context['op_0'] = 'Venta'
        context['op_1'] = 'Alquiler'
        context['categorymenu'] = range(1, 10)
        context['totalsuggestions'] = 200
        context['suggestionsindex'] = 30
        return context


class ListResultView(ListView):
    model = Ad
    paginate_by = 2
    template_name = "results.html"
    context_object_name = "ad"

    def get_context_data(self, **kwargs):
        context = super(ListResultView, self).get_context_data(**kwargs)
        context['metatitle'] = 'Lista de propiedades en Peru'
        context['addthis_account'] = 'ra-51070c8464c9a004'
        context['maxpages'] = 13
        context['rangepages'] = range(13)
        context['currency_23'] = 'S./'
        context['currency_24'] = 'US$'
        context['ini'] = 1
        return context

    def get_queryset(self):
        queryset = Ad.objects.all()
        return queryset

    def define_pages(total):
        ini = 1
        end = 13
        current = request.HttpRequest.GET
        if current == 0:
            current = 1
        if total <= 13:
            end = total
        if current > 6:
            if total < current + 5:
                ini = total - 13
                end = total
            else:
                ini = current - 5
                end = current + 5
        return {'start': ini, 'finish': end, 'crr': current}


class DetailedPageView(TemplateView):
    template_name = 'details.html'
    queryset = Ad.objects.all()

    def get_context_data(self, **kwargs):
        context = super(DetailedPageView, self).get_context_data(**kwargs)
        context['metatitle'] = 'Pagina de detalle con toda la informacion del anuncio'
        context['addthis_account'] = 'ra-51070c8464c9a004'
        context['prop'] = Ad.objects.all()
        return context

    def get_object(self):
        object = super(DetailedPageView, self).get_object()
        return object

class MoreSuggestionsView(TemplateView):
    template_name = "moresuggestions.html"

    def get_context_data(self, **kwargs):
        context = super(MoreSuggestionsView, self).get_context_data(**kwargs)
        context['metatitle'] = 'listado de sugerencias'
        context['totalsuggestions'] = 200
        context['suggestionsperpage'] = 30
        return context
