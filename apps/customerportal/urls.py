from django.conf.urls import url

from views import HomePageView, ListResultView, DetailedPageView, MoreSuggestionsView

urlpatterns = [
    url('^$', HomePageView.as_view(), name='home'),
    url('^results.html$', ListResultView.as_view(), name='results'),
    url('^details.html$', DetailedPageView.as_view(), name='details'),
    url('^moresuggestions.html', MoreSuggestionsView.as_view(), name='moresuggestions'),
]
