# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Ad',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('idproperty', models.CharField(max_length=20)),
                ('idbroker', models.CharField(max_length=20)),
                ('activity', models.IntegerField(default=0, null=True)),
                ('othergeo', models.CharField(max_length=50, null=True)),
                ('address', models.CharField(max_length=250, null=True)),
                ('photos', models.TextField(null=True, blank=True)),
                ('map', models.CharField(max_length=250, null=True)),
                ('video', models.CharField(max_length=250, null=True)),
                ('title', models.CharField(max_length=70, null=True)),
                ('description', models.TextField()),
                ('telephone', models.CharField(max_length=50, null=True)),
                ('cellphone', models.CharField(max_length=50, null=True)),
                ('email', models.CharField(max_length=100)),
                ('bedrooms', models.IntegerField(default=0, null=True)),
                ('bathrooms', models.IntegerField(default=0, null=True)),
                ('at', models.CharField(max_length=50, null=True)),
                ('ac', models.CharField(max_length=50, null=True)),
                ('rent_price', models.FloatField()),
                ('sale_price', models.FloatField()),
                ('currency', models.IntegerField(default=0, null=True)),
                ('noproperty', models.IntegerField()),
                ('timeing', models.IntegerField()),
            ],
            options={
                'db_table': 'ad',
            },
        ),
        migrations.CreateModel(
            name='Category',
            fields=[
                ('idcategory', models.IntegerField(unique=True, serialize=False, primary_key=True)),
                ('name', models.CharField(max_length=200)),
                ('slug', models.SlugField()),
                ('description', models.TextField(default='')),
            ],
            options={
                'db_table': 'category',
                'verbose_name_plural': 'categories',
            },
        ),
        migrations.CreateModel(
            name='Location',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=64)),
                ('order', models.IntegerField()),
                ('seoname', models.CharField(max_length=145)),
                ('description', models.TextField(null=True, blank=True)),
                ('last_modified', models.DateTimeField(null=True, blank=True)),
                ('has_image', models.IntegerField()),
                ('latitude', models.FloatField(null=True, blank=True)),
                ('longitude', models.FloatField(null=True, blank=True)),
            ],
            options={
                'db_table': 'locations',
            },
        ),
        migrations.AddField(
            model_name='ad',
            name='idcategory',
            field=models.ForeignKey(to='classifieds.Category'),
        ),
        migrations.AddField(
            model_name='ad',
            name='idgeo',
            field=models.ForeignKey(to='classifieds.Location'),
        ),
    ]
