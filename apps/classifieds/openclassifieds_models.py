# This is an auto-generated Django model module.
# You'll have to do the following manually to clean this up:
#   * Rearrange models' order
#   * Make sure each model has one field with primary_key=True
#   * Remove `managed = False` lines if you wish to allow Django to create, modify, and delete the table
# Feel free to rename the models, but don't rename db_table values or field names.
#
# Also note: You'll have to insert the output of 'django-admin sqlcustom [app_label]'
# into your database.
from __future__ import unicode_literals

from django.db import models


class Oc2Access(models.Model):
    id_access = models.AutoField(primary_key=True)
    id_role = models.IntegerField()
    access = models.CharField(max_length=100)

    class Meta:
        managed = False
        db_table = 'oc2_access'


class Oc2Ads(models.Model):
    id_ad = models.AutoField(primary_key=True)
    id_user = models.ForeignKey('Oc2Users', db_column='id_user')
    id_category = models.ForeignKey('Oc2Categories', db_column='id_category')
    id_location = models.IntegerField()
    title = models.CharField(max_length=145)
    seotitle = models.CharField(unique=True, max_length=145)
    description = models.TextField()
    address = models.CharField(max_length=145, blank=True, null=True)
    latitude = models.FloatField(blank=True, null=True)
    longitude = models.FloatField(blank=True, null=True)
    price = models.DecimalField(max_digits=14, decimal_places=3)
    phone = models.CharField(max_length=30, blank=True, null=True)
    website = models.CharField(max_length=200, blank=True, null=True)
    ip_address = models.BigIntegerField(blank=True, null=True)
    created = models.DateTimeField()
    published = models.DateTimeField(blank=True, null=True)
    featured = models.DateTimeField(blank=True, null=True)
    last_modified = models.DateTimeField(blank=True, null=True)
    status = models.IntegerField()
    has_images = models.IntegerField()
    stock = models.IntegerField(blank=True, null=True)
    rate = models.FloatField(blank=True, null=True)
    favorited = models.IntegerField()

    class Meta:
        managed = False
        db_table = 'oc2_ads'


class Oc2Categories(models.Model):
    id_category = models.AutoField(primary_key=True)
    name = models.CharField(max_length=145)
    order = models.IntegerField()
    created = models.DateTimeField()
    id_category_parent = models.IntegerField()
    parent_deep = models.IntegerField()
    seoname = models.CharField(unique=True, max_length=145)
    description = models.TextField(blank=True, null=True)
    price = models.DecimalField(max_digits=10, decimal_places=2)
    last_modified = models.DateTimeField(blank=True, null=True)
    has_image = models.IntegerField()

    class Meta:
        managed = False
        db_table = 'oc2_categories'


class Oc2Config(models.Model):
    group_name = models.CharField(max_length=128)
    config_key = models.CharField(primary_key=True, max_length=128)
    config_value = models.TextField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'oc2_config'
        unique_together = (('group_name', 'config_key'),)


class Oc2Content(models.Model):
    id_content = models.AutoField(primary_key=True)
    locale = models.CharField(max_length=8)
    order = models.IntegerField()
    title = models.CharField(max_length=145)
    seotitle = models.CharField(max_length=145)
    description = models.TextField(blank=True, null=True)
    from_email = models.CharField(max_length=145, blank=True, null=True)
    created = models.DateTimeField()
    type = models.CharField(max_length=5)
    status = models.IntegerField()

    class Meta:
        managed = False
        db_table = 'oc2_content'


class Oc2Coupons(models.Model):
    id_coupon = models.AutoField(primary_key=True)
    id_product = models.IntegerField(blank=True, null=True)
    name = models.CharField(unique=True, max_length=145)
    notes = models.CharField(max_length=245, blank=True, null=True)
    discount_amount = models.DecimalField(max_digits=14, decimal_places=3)
    discount_percentage = models.DecimalField(max_digits=14, decimal_places=3)
    number_coupons = models.IntegerField(blank=True, null=True)
    valid_date = models.DateTimeField(blank=True, null=True)
    created = models.DateTimeField()
    status = models.IntegerField()

    class Meta:
        managed = False
        db_table = 'oc2_coupons'


class Oc2Crontab(models.Model):
    id_crontab = models.AutoField(primary_key=True)
    name = models.CharField(unique=True, max_length=50)
    period = models.CharField(max_length=50)
    callback = models.CharField(max_length=140)
    params = models.CharField(max_length=255, blank=True, null=True)
    description = models.CharField(max_length=255, blank=True, null=True)
    date_created = models.DateTimeField()
    date_started = models.DateTimeField(blank=True, null=True)
    date_finished = models.DateTimeField(blank=True, null=True)
    date_next = models.DateTimeField(blank=True, null=True)
    times_executed = models.BigIntegerField(blank=True, null=True)
    output = models.CharField(max_length=50, blank=True, null=True)
    running = models.IntegerField()
    active = models.IntegerField()

    class Meta:
        managed = False
        db_table = 'oc2_crontab'


class Oc2Favorites(models.Model):
    id_favorite = models.AutoField(primary_key=True)
    id_user = models.IntegerField()
    id_ad = models.IntegerField()
    created = models.DateTimeField()

    class Meta:
        managed = False
        db_table = 'oc2_favorites'


class Oc2Forums(models.Model):
    id_forum = models.AutoField(primary_key=True)
    name = models.CharField(max_length=145)
    order = models.IntegerField()
    created = models.DateTimeField()
    id_forum_parent = models.IntegerField()
    parent_deep = models.IntegerField()
    seoname = models.CharField(unique=True, max_length=145)
    description = models.CharField(max_length=255, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'oc2_forums'


class Oc2Locations(models.Model):
    id_location = models.AutoField(primary_key=True)
    name = models.CharField(max_length=64)
    order = models.IntegerField()
    id_location_parent = models.IntegerField()
    parent_deep = models.IntegerField()
    seoname = models.CharField(unique=True, max_length=145)
    description = models.TextField(blank=True, null=True)
    last_modified = models.DateTimeField(blank=True, null=True)
    has_image = models.IntegerField()
    latitude = models.FloatField(blank=True, null=True)
    longitude = models.FloatField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'oc2_locations'


class Oc2Messages(models.Model):
    id_message = models.AutoField(primary_key=True)
    id_ad = models.IntegerField(blank=True, null=True)
    id_message_parent = models.IntegerField(blank=True, null=True)
    id_user_from = models.IntegerField()
    id_user_to = models.IntegerField()
    message = models.TextField()
    price = models.DecimalField(max_digits=14, decimal_places=3)
    read_date = models.DateTimeField(blank=True, null=True)
    created = models.DateTimeField()
    status = models.IntegerField()

    class Meta:
        managed = False
        db_table = 'oc2_messages'


class Oc2Orders(models.Model):
    id_order = models.AutoField(primary_key=True)
    id_user = models.IntegerField()
    id_ad = models.IntegerField(blank=True, null=True)
    id_product = models.CharField(max_length=20)
    id_coupon = models.IntegerField(blank=True, null=True)
    paymethod = models.CharField(max_length=20, blank=True, null=True)
    created = models.DateTimeField()
    pay_date = models.DateTimeField(blank=True, null=True)
    currency = models.CharField(max_length=3)
    amount = models.DecimalField(max_digits=14, decimal_places=3)
    status = models.IntegerField()
    description = models.CharField(max_length=145, blank=True, null=True)
    txn_id = models.CharField(max_length=255, blank=True, null=True)
    featured_days = models.IntegerField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'oc2_orders'


class Oc2Posts(models.Model):
    id_post = models.AutoField(primary_key=True)
    id_user = models.IntegerField()
    id_post_parent = models.IntegerField(blank=True, null=True)
    id_forum = models.IntegerField(blank=True, null=True)
    title = models.CharField(max_length=245)
    seotitle = models.CharField(unique=True, max_length=245)
    description = models.TextField()
    created = models.DateTimeField()
    ip_address = models.BigIntegerField(blank=True, null=True)
    status = models.IntegerField()

    class Meta:
        managed = False
        db_table = 'oc2_posts'


class Oc2Reviews(models.Model):
    id_review = models.AutoField(primary_key=True)
    id_user = models.IntegerField()
    id_ad = models.IntegerField()
    rate = models.IntegerField()
    description = models.CharField(max_length=1000)
    created = models.DateTimeField()
    ip_address = models.BigIntegerField(blank=True, null=True)
    status = models.IntegerField()

    class Meta:
        managed = False
        db_table = 'oc2_reviews'


class Oc2Roles(models.Model):
    id_role = models.AutoField(primary_key=True)
    name = models.CharField(unique=True, max_length=45, blank=True, null=True)
    description = models.CharField(max_length=245, blank=True, null=True)
    date_created = models.DateTimeField()

    class Meta:
        managed = False
        db_table = 'oc2_roles'


class Oc2Subscribers(models.Model):
    id_subscribe = models.AutoField(primary_key=True)
    id_user = models.IntegerField()
    id_category = models.IntegerField()
    id_location = models.IntegerField()
    min_price = models.DecimalField(max_digits=14, decimal_places=3)
    max_price = models.DecimalField(max_digits=14, decimal_places=3)
    created = models.DateTimeField()

    class Meta:
        managed = False
        db_table = 'oc2_subscribers'


class Oc2Users(models.Model):
    id_user = models.AutoField(primary_key=True)
    name = models.CharField(max_length=145, blank=True, null=True)
    seoname = models.CharField(unique=True, max_length=145, blank=True, null=True)
    email = models.CharField(unique=True, max_length=145)
    password = models.CharField(max_length=64)
    description = models.TextField(blank=True, null=True)
    status = models.IntegerField()
    id_role = models.IntegerField(blank=True, null=True)
    id_location = models.IntegerField(blank=True, null=True)
    created = models.DateTimeField()
    last_modified = models.DateTimeField(blank=True, null=True)
    logins = models.IntegerField()
    last_login = models.DateTimeField(blank=True, null=True)
    last_ip = models.BigIntegerField(blank=True, null=True)
    user_agent = models.CharField(max_length=40, blank=True, null=True)
    token = models.CharField(unique=True, max_length=40, blank=True, null=True)
    token_created = models.DateTimeField(blank=True, null=True)
    token_expires = models.DateTimeField(blank=True, null=True)
    api_token = models.CharField(unique=True, max_length=40, blank=True, null=True)
    hybridauth_provider_name = models.CharField(max_length=40, blank=True, null=True)
    hybridauth_provider_uid = models.CharField(max_length=245, blank=True, null=True)
    subscriber = models.IntegerField()
    rate = models.FloatField(blank=True, null=True)
    has_image = models.IntegerField()
    failed_attempts = models.IntegerField()
    last_failed = models.DateTimeField(blank=True, null=True)
    notification_date = models.DateTimeField(blank=True, null=True)
    device_id = models.CharField(max_length=255, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'oc2_users'
        unique_together = (('hybridauth_provider_name', 'hybridauth_provider_uid'),)


class Oc2Visits(models.Model):
    id_visit = models.AutoField(primary_key=True)
    id_ad = models.IntegerField(blank=True, null=True)
    id_user = models.IntegerField(blank=True, null=True)
    contacted = models.IntegerField()
    created = models.DateTimeField()
    ip_address = models.BigIntegerField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'oc2_visits'
