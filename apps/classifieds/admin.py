"""
"""

from django.contrib import admin

import models


class AdAdmin(admin.ModelAdmin):
    list_filter = ('activity', 'idcategory',)
    list_display = ('title', 'idcategory', 'timeing',)
    search_fields = ('title',)


class CategoryAdmin(admin.ModelAdmin):
    prepopulated_fields = {'slug': ('name',)}


admin.site.register(models.Ad, AdAdmin)
admin.site.register(models.Category, CategoryAdmin)
