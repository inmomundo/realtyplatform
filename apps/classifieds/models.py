# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.db import models

# TODO: Implement Modified Preorder Tree Traversal
# TODO: Categorias por sitio o país
# TODO: Alias o mapeo de categorías
# TODO: Iniform model attributes to name convention
# XXX: Keep PEP8 code style conventions


class Category(models.Model):
    idcategory = models.IntegerField(primary_key=True, unique=True)
    name = models.CharField(max_length=200)
    slug = models.SlugField()
    description = models.TextField(default='')

    def __unicode__(self):
        return self.name + u' Category'

    class Meta:
        verbose_name_plural = u'categories'
        db_table = 'category'


# TODO: Implement Modified Preorder Tree Traversal
class Location(models.Model):
    # TODO: reamplazar por referencia ciclica mediante fk
    # parent = models.IntegerField()
    name = models.CharField(max_length=64)
    order = models.IntegerField()
    seoname = models.CharField(max_length=145)
    description = models.TextField(blank=True, null=True)
    last_modified = models.DateTimeField(blank=True, null=True)
    has_image = models.IntegerField()
    latitude = models.FloatField(blank=True, null=True)
    longitude = models.FloatField(blank=True, null=True)

    class Meta:
        db_table = 'locations'

# TODO: Implement Entity-Attribute-Value storage
class Ad(models.Model):
    idproperty = models.CharField(max_length=20, primary_key=True)
    idcategory = models.ForeignKey(Category)
    # TODO: idbroker debe ser un ForeignKey de la tabla broker que aun no existe
    idbroker = models.CharField(max_length=20)
    activity = models.IntegerField(default=0, null=True)
    idgeo = models.ForeignKey(Location)
    othergeo = models.CharField(max_length=50, null=True)
    address = models.CharField(max_length=250, null=True)
    photos = models.TextField(blank=True, null=True)
    map = models.CharField(max_length=250, null=True)
    video = models.CharField(max_length=250, null=True)
    title = models.CharField(max_length=70, null=True)
    description = models.TextField()
    telephone = models.CharField(max_length=50, null=True)
    cellphone = models.CharField(max_length=50, null=True)
    email = models.CharField(max_length=100)
    bedrooms = models.IntegerField(default=0, null=True)
    bathrooms = models.IntegerField(default=0, null=True)
    at = models.CharField(max_length=50, null=True)
    ac = models.CharField(max_length=50, null=True)
    rent_price = models.FloatField()
    sale_price = models.FloatField()
    currency = models.IntegerField(default=0, null=True)
    noproperty = models.IntegerField()
    timeing = models.IntegerField()

    def __unicode__(self):
        return u'Ad #' + unicode(
            self.pk) + ' titled "' + self.title + u'" in category ' + self.category.name + u'" located in ' + self.locations.name

    class Meta:
        db_table = 'ad'

# class AdImage(models.Model):
#    ad = models.ForeignKey(Ad)
#    full_photo = models.ImageField(upload_to='classifieds/ad/', blank=True)
#
#    class Meta:
#        db_table = 'ad_image'
