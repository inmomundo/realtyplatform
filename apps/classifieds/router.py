class ClassifiedsRouter(object):
    def db_for_read(self, model, **hints):
        "Point all operations on classifieds models to 'classifieds'"
        if model._meta.app_label == 'classifieds':
            return 'classifieds'
        return None

    def db_for_write(self, model, **hints):
        "Point all operations on classifieds models to 'classifieds'"
        if model._meta.app_label == 'classifieds':
            return 'classifieds'
        return None
