"""
settings for development mode
"""
from django.conf.global_settings import TEMPLATE_CONTEXT_PROCESSORS  # noqa
from settings.common import *  # noqa
from django.utils.translation import ugettext_lazy as _


DATA_DIR = os.path.join(PROJECT_DIR, 'data')

if not os.path.exists(DATA_DIR):
    os.makedirs(DATA_DIR)

DEBUG = True
TEMPLATE_DEBUG = DEBUG

LANGUAGES = (
  ('es', _('Spanish')),
  ('en', _('English')),
)

ROOT_URLCONF = 'urls.project.dev'

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.mysql',
        'NAME': 'realtyplatform_dev',
        'USER': 'realtyplatform',
        'PASSWORD': 'R34l7y',
        'HOST': '104.238.133.30',
        'PORT': '3306',
    },
    'inmoperu': {
        'ENGINE': 'django.db.backends.mysql',
        'NAME': 'inmo_inmope',
        'USER': 'inmo_inmope',
        'PASSWORD': 'fnA9TZz=EGA]',
        'HOST': '209.222.19.219',
        'PORT': '3306',
    },
}

DATABASE_ROUTERS = [
    'apps.inmo.inmoperu.router.InmoPeruRouter',
    # 'apps.classifieds.router.ClassifiedsRouter'
]

EMAIL_BACKEND = 'django.core.mail.backends.console.EmailBackend'

INTERNAL_IPS = ('127.0.0.1','186.77.168.191','104.238.133.30','10.99.0.11')

MIDDLEWARE_CLASSES += (
    'debug_toolbar.middleware.DebugToolbarMiddleware',
)


INSTALLED_APPS += (
    'django.contrib.admin',
    'debug_toolbar',
    'django_nose',
    # 'rosetta',
    'apps.inmo.inmoperu',
    'apps.customerportal',
    'apps.classifieds',
)

# omit the inclusion of a sitemap link
ROBOTS_USE_SITEMAP = False

# Use nose to run all tests
TEST_RUNNER = 'django_nose.NoseTestSuiteRunner'
