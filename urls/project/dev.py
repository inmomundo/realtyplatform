"""
project specic settings for urls in developing mode
"""

from urls.dev import *  # noqa

urlpatterns += (
    url(r'', include('apps.customerportal.urls')),
)
