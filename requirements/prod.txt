# requirements/prod.txt: production dependency requirements
-r common.txt

# middleware utility that allows you to properly serve static assets from production with a WSGI server like Gunicorn
# dj-static
# psycopg2
MySQL-python==1.2.5
# Serve static or templated content via WSGI or stand-alone
# static

-r project/common.txt
-r project/prod.txt
